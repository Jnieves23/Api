'use strict'
var fs = require('fs');
var path = require('path');

var Category= require('../models/category');

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getCategory(req, res){
    var categoryId= req.params.id;
    Category.findById(categoryId,(err,category)=>{
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }
        else{
            if(!category){
                res.status(404).send({message: 'La categoria no existe'});
            }
            else{
                res.status(200).send({category:category});
            }
        }
    });
  
}

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveCategory(req,res){
    var category = new Category();

    var params = req.body;
    category.title= params.title;
    category.description= params.description;
    category.estado='S';

    category.save((err,categoryStored)=>{
        if(err){
            req.status(500).send({message:'Error al guardar el horario'});
        }
        else{
            if(!categoryStored){
                req.status(404).send({message: 'La categoría no ha sido guardada'});
            }
            else{
                res.status(200).send({category: categoryStored});
            }
        }
    });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getCategorys_page(req, res){

    var page = req.params.page || 1;
    var itemsPerPage = Number(req.params.itemsperpage) || 3;
    var sort = req.params.sort || 'name';
    


    Category.find(/* {estado:'S'} */).sort(sort).paginate( page,itemsPerPage, function (err, categorys,total){
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }else{
                if(!categorys){
                    res.status(404).send({message:'No hay categorias'});
                }else{
                    return res.status(200).send({total_items: total, categorys:  categorys});
                }
        }
    });

}

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getCategorys_sin_page(req, res){

  

    Category.find({estado:'S' }).exec(function (err, categorys){
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }else{
                if(!categorys){
                    res.status(404).send({message:'No hay categorias'});
                }else{
                    return res.status(200).send({categorys:categorys});
                }
        }
    });

}
/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateCategory(req, res){
    var categoryId= req.params.id;
    var update = req.body;
//findOneAndUpdate
    Category.findByIdAndUpdate(categoryId,update, function(err, categoryUpdated){
   
    if(err){
        res.status(500).send({message: 'Error al guardar el horario'});
    }
    else{
        if(!categoryUpdated){
            res.status(404).send({message: 'La categoría no ha sido actualizada'});
        }
        else{
            res.status(200).send({category: categoryUpdated});
        }
    } });

}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function deleteCategory( req, res){
    var categoryId = req.params.id;

    Category.findByIdAndRemove(categoryId,function(err, categoryIdRemoved){
        if(err){
            res.status(500).send({message: ' Error al eliminar el horario'});
        }else{
                if(!categoryIdRemoved){
                    res.status(404).send({message: 'El horario no ha sido eliminado'});
                }else{
                    res.status(200).send({category: categoryIdRemoved});
                       
                }
        }
    });
}


module.exports={
    getCategory,
    saveCategory,
    getCategorys_page,
    getCategorys_sin_page,
    updateCategory,
    deleteCategory,
}