'use strict'
var fs = require('fs');
var path = require('path');

var User= require('../models/user');
var ConfigMensaje= require('./configMensaje');
var bcrypt= require('bcrypt-nodejs');
var jwt= require('../services/jwt');

var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'sajdjhabsda6s5d165as4d56a4s65d1as5d1as65d651asd';


function pruebas(req,res){
   res.status(200).send({message:'Controlador user'});
   // res.status(200).send({message:req.params.id});
}

/**
 * *********************************************************************
 * METODO: encrypt
 * DESCRIPPCION: ENCRIPTA LA CONTRASEÑA
 * AUTOR: Jnieves
 * FECHA CREACION: 06/01/2019
 * ************************************************************************
 */
function encrypt(text){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
 /**
 * *********************************************************************
 * METODO: decrypt
 * DESCRIPPCION: DESENCRIPTA LA CONTRASEÑA
 * AUTOR: Jnieves
 * FECHA CREACION: 06/01/2019
 * ************************************************************************
 */
function decrypt(text){
  var decipher = crypto.createDecipher(algorithm,password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}
/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveUser(req, res){
    var user= new User();

    var params = req.body;
    console.log(params);

    user.name = params.name;
    user.surname= params.surname;
    user.email= params.email;
    user.role= params.role;
    user.image= 'null';
    user.office=params.office;
    user.direccion=params.direccion;
    user.identification=params.identification;
    user.usuario=params.usuario;
    user.estado='S';


    if(params.password){
        //Encriptar contraseña 
     //   bcrypt.hash(params.password,null,null,function(err,hash){
     //       user.password= hash;
            user.password=encrypt(params.password);
            if(user.name != null && user.email !=null){
         //   if(user.name != null && user.surname!=null && user.email !=null){
                user.save((err, userStored)=>{
                   if(err){
                       res.status(500).send({message: 'Error al guardar el usuario'});
                   }
                   else{
                     if(!userStored){
                        res.status(404).send({message: 'No se ha registrado el usuario'});
                     }
                     else{
                        res.status(200).send({user: userStored});
                     }
                   }
                });
            }
            else{
                res.status(200).send({message:'Rellena todos los campos'});
            }
     //   });
    }
    else{
        res.status(200).send({message: 'Introduce la contraseña '});
    }
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function forgetPassword(req, res){
    var correo = req.params.correo;

    //1. BUSCAR EL CORREO
    User.findOne({email:correo.toLowerCase()}).exec(function (err, user) {
        if(err){
            res.status(500).send({message:'Error en la petición de Recuperar la contraseña'});
        }
        else{
            if(! user || User==""){
                res.status(404).send({message: 'El correo es incorrecto'});
            }
            else{
                /* SI EXISTE EL CORREO SE ACTUALIZARÁ LA CONTRASEÑA*/

                //2. GENERAR NUEVA CONTRASEÑA
                var password= Math.random().toString(36).substring(7);
                //res.status(200).send({user});

                //3. ACTUALIZA MI CONTRASEÑA
                    console.log("user ",user._id);
                     var update = user;
                    update.password=encrypt(password);//Encriptar y asignar la nueva contraseña random encriptada

                    User.findByIdAndUpdate(user._id,update, (err,userUpdate)=>{
                        if(err){
                            res.status(500).send({message: 'Error en la petición de actualizar de Recuperar contraseña'});
                        }
                        else{
                            if(!userUpdate || userUpdate==""){
                                res.status(404).send({message: 'El correo es incorrecto'});
                            }
                            else{
                                 //4. ENVIAR CORREO DE NOTIFICACION
                                console.log("userUpdate",userUpdate);
                                 var mailData = {
                                    "motivo" : "ForgetPassword",
                                    "email" : userUpdate.email,
                                    "asunto" : "Recuperar contraseña",
                                    "password": password,
                                    "nombre": userUpdate.name
                                };
                                console.log("mailData",mailData);

                                var resultado=ConfigMensaje.SedMail(mailData, function(error, respuesta){
                                    console.log("respuesta,",respuesta);
                                    if(error){
                                        console.log(respuesta);
                                        res.status(500).send({message: 'Error en la petición de actualizar de Recuperar contraseña'});
                                    }
                                    else{
                                        if(!respuesta || respuesta==""){
                                            res.status(404).send({message: 'Ocurrió un error en el envío del correo'});
                                        }else{
                                            res.status(200).send({message:"Se ha enviado la nueva contraseña a su correo"});
                                        }

                                    }
                                });//CONFIGMENSAJE
                               
                                    res.status(200).send({message:"Se ha enviado la nueva contraseña a su correo"});
                               
                               
                                
                        }
                        }
                    }); 
               


            } 
        }
       
    });

  
    //2. ENVIAR CORREO CON LA NUEVA CONTRASEÑA

    
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveClientFisicamente(req, res){
    var user= new User();

    var params = req.body;
    console.log(params);

    user.name = params.name;
    user.surname= params.surname;
    user.email= params.email;
    user.role= params.role;
    user.image= 'null';
    user.office=params.office;

    user.identification=params.identification;
    user.usuario=params.usuario;
    user.estado='S';

                user.save((err, userStored)=>{
                   if(err){
                       res.status(500).send({message: 'Error al guardar el usuario'});
                   }
                   else{
                     if(!userStored){
                        res.status(404).send({message: 'No se ha registrado el usuario'});
                     }
                     else{
                        res.status(200).send({user: userStored});
                     }
                   }
                });
    
    
}







/***************
 * METODO     : loginUser
 * DESCRIPCION: RETORNA UN TOKEN 
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function loginUser(req, res){
    var params= req.body;
    var email= params.email;
    var password= params.password
    var usuario= params.usuario;

    var find;
    if(usuario){
        find= User.findOne({usuario:usuario});
    }
    else{
        find=  User.findOne({email:email.toLowerCase()});
    }
    console.log("password " ,password);
    find.exec(function (err, user) {
        if(err){
            res.status(500).send({message:'Error en la petición de Login'});
        }
        else{
            if(! user){
                res.status(404).send({message: 'El usuario no existe'});
            }
            else{
                // comparar la contraseña
            //    bcrypt.compare(password,user.password, function(err,check){
                    if(decrypt(user.password)==password){
                        //devolver los datos del usuario logeado
                        if(params.gethash){
                            console.log("se va por el gethash");
                            //devolver un tocken de jwt
                            res.status(200).send({
                               token: jwt.createToken(user)
                            });
                        }
                        else{
                            console.log("se va por otro lado");
                            console.log(user);
                            user.password=password;
                            res.status(200).send({user});
                        }
                    }
                    else{
                        res.status(404).send({message: 'Estas credenciales no coinciden con nuestros registros'});
                    }
            //   });
            }
        }
    });
}


/***************
 * METODO     : loginUser
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function updateUser(req,res){
    var userId =req.params.id;
    var update = req.body;
    if(userId !=req.user.sub){
        return res.status(500).send({message: 'No tienes permiso para actualizar este usuario'});
    }

   
   update.password=encrypt(update.password);
    User.findByIdAndUpdate(userId,update, (err,userUpdate)=>{
        if(err){
            res.status(500).send({message: 'Error al actualizar el usuario'});
        }
        else{
            if(!userUpdate){
                res.status(404).send({message: 'No se ha podido actualizar el usuario'});
            }
            else{
                res.status(200).send({user: userUpdate});
            }
        }
    }); 
}


/***************
 * METODO     : updateUser_sin_permisos
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function updateUser_sin_permisos(req,res){
    var userId =req.params.id;
    var update = req.body;
/*     if(userId !=req.user.sub){
        return res.status(500).send({message: 'No tienes permiso para actualizar este usuario'});
    } */
    if(!userId){
        return res.status(500).send({message: 'Ingrese la información'});
    }
   
   update.password=encrypt(update.password);
    User.findByIdAndUpdate(userId,update, (err,userUpdate)=>{
        if(err){
            res.status(500).send({message: 'Error al actualizar el usuario'});
        }
        else{
            if(!userUpdate){
                res.status(404).send({message: 'No se ha podido actualizar el usuario'});
            }
            else{
                res.status(200).send({user: userUpdate});
            }
        }
    }); 
}



/***************
 * METODO     : loginUser
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function uploadImage(req,res){
    var userId= req.params.id;
    var filename='No subido...';

    if(req.files){
        var file_path= req.files.image.path;
        console.log(file_path);
        
        var file_split=file_path.split('\\');
        var file_name=file_split[2];
        var exr_explit= file_name.split('\.');
        var file_ext=exr_explit[1];//extención

        if(file_ext =='jpeg'||file_ext =='png' || file_ext=='jpg'|| file_ext =='gif'){
            User.findByIdAndUpdate(userId,{image:file_name }, (err,userUpdate)=>{
                if(err){
                    res.status(500).send({message: 'No se ha podido cargar el fichero'});
                }
                else{
                    if(!userUpdate){
                    res.status(404).send({message: 'No se ha encontrado el usuario'});
                    }
                    else{
                        res.status(200).send({image:file_name, user: userUpdate});
                    }
                }
            });
        }
        else{
            res.status(200).send({message: 'Extención de la imagen no valida'});
        }
    }
    else{
        res.status(200).send({message: 'No has subido ninguna imagen..'});
    }
}

/***************
 * METODO     : getImageFile
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function getImageFile(req, res){
    console.log("getImageFile");
    var image_file= req.params.imageFile;
    var path_file = './uploads/users/'+image_file;
    fs.exists(path_file,function(exists){
        if(exists){
            res.sendFile (path.resolve(path_file));
        }
        else{
            res.status(200).send({message: 'No existe la imagen'});        
        }
    });
}


/***************
 * METODO     : getImageFile
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function getFindIdentification(req, res){
    
    
    
    var identificationId= req.params.cedula;

    if(!identificationId){
        return res.status(500).send({message: 'Por favor ingrese la identificación'});
    }
    
    User.find({identification: identificationId}).exec(function (err, userData){
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }else{
                if(!userData){
                    res.status(404).send({message:'Usuario Nuevo'});
                }else{
                    if(userData[0] && userData[0].password){
                        userData[0].password=(decrypt(userData[0].password));
                    }

                    
                    //receipt[0].user.password=(decrypt(receipt[0].user.password));
                    return res.status(200).send({user: userData});
                }
        }
    });
  
  }

/***************
 * METODO     : getImageFile
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function getFindId(req, res){
    
    
    
    var Id= req.params.id;
    console.log(req.params, Id);

    if(!Id){
        return res.status(500).send({message: 'No hay ningún dato para procesar'});
    }
    
    User.findById(Id,(err,userData)=>{
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }else{
                if(!userData){
                    res.status(404).send({message:'Usuario Nuevo'});
                }else{
                    return res.status(200).send({user: userData});
                }
        }
    });
  
  }


 function mValidateCedulaEcuadorian(req, res){
     
    /**
     * 1.- Se debe validar que tenga 10 numeros
     * 2.- Se extrae los dos primero digitos de la izquierda y compruebo que existan las regiones
     * 3.- Extraigo el ultimo digito de la cedula
     * 4.- Extraigo Todos los pares y los sumo
     * 5.- Extraigo Los impares los multiplico x 2 si el numero resultante es mayor a 9 le restamos 9 al resultante
     * 6.- Extraigo el primer Digito de la suma (sumaPares + sumaImpares)
     * 7.- Conseguimos la decena inmediata del digito extraido del paso 6 (digito + 1) * 10
     * 8.- restamos la decena inmediata - suma / si la suma nos resulta 10, el decimo digito es cero
     * 9.- Paso 9 Comparamos el digito resultante con el ultimo digito de la cedula si son iguales todo OK sino existe error.     
     */

    //var cedula = '0931811087';
    var cedula = req.params.cedula;

    if(!cedula){
        return res.status(500).send({message: 'Debe ingresar la cédula'});
    }

    console.log( req.params.cedula);

    //Preguntamos si la cedula consta de 10 digitos
    if(cedula.length == 10){
       
       //Obtenemos el digito de la region que son los dos primeros digitos
       var digito_region = cedula.substring(0,2);
       
       //Pregunto si la region existe ecuador se divide en 24 regiones
       if( digito_region >= 1 && digito_region <=24 ){
         
         // Extraigo el ultimo digito
         var ultimo_digito   = cedula.substring(9,10);

         //Agrupo todos los pares y los sumo
         var pares = parseInt(cedula.substring(1,2)) + parseInt(cedula.substring(3,4)) + parseInt(cedula.substring(5,6)) + parseInt(cedula.substring(7,8));

         //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
         var numero1 = cedula.substring(0,1);
         var numero1 = (numero1 * 2);
         if( numero1 > 9 ){ var numero1 = (numero1 - 9); }

         var numero3 = cedula.substring(2,3);
         var numero3 = (numero3 * 2);
         if( numero3 > 9 ){ var numero3 = (numero3 - 9); }

         var numero5 = cedula.substring(4,5);
         var numero5 = (numero5 * 2);
         if( numero5 > 9 ){ var numero5 = (numero5 - 9); }

         var numero7 = cedula.substring(6,7);
         var numero7 = (numero7 * 2);
         if( numero7 > 9 ){ var numero7 = (numero7 - 9); }

         var numero9 = cedula.substring(8,9);
         var numero9 = (numero9 * 2);
         if( numero9 > 9 ){ var numero9 = (numero9 - 9); }

         var impares = numero1 + numero3 + numero5 + numero7 + numero9;

         //Suma total
         var suma_total = (pares + impares);

         //extraemos el primero digito
         var primer_digito_suma = String(suma_total).substring(0,1);

         //Obtenemos la decena inmediata
         var decena = (parseInt(primer_digito_suma) + 1)  * 10;

         //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
         var digito_validador = decena - suma_total;

         //Si el digito validador es = a 10 toma el valor de 0
         if(digito_validador == 10)
           var digito_validador = 0;

         //Validamos que el digito validador sea igual al de la cedula
         if(digito_validador == ultimo_digito){
            return res.status(200).send({message: 1});
         
         }else{
            return res.status(200).send({message: 'la cedula:' + cedula + ' es incorrecta'});
         }
         
       }else{
         // imprimimos en consola si la region no pertenece
         return res.status(200).send({message: 'Esta cedula no pertenece a ninguna region'});
       }
    }
    if(cedula.length > 10){
        return res.status(200).send({message: 'Esta cedula tiene más de 10 Digitos'});
    }
    
    if(cedula.length < 10){
       //imprimimos en consola si la cedula tiene mas o menos de 10 digitos
       return res.status(200).send({message: 'Esta cedula tiene menos de 10 Digitos'});
    }    
 

  }

module.exports= {
    pruebas,
    saveUser,
    loginUser,
    updateUser,
    updateUser_sin_permisos,
    uploadImage,
    getImageFile,
    getFindIdentification,
    saveClientFisicamente,
    getFindId,
    forgetPassword,
    mValidateCedulaEcuadorian
};