'use strict'
var fs = require('fs');
var path = require('path');

var  User= require('../models/user');
var Author= require('../models/author');
var Office= require('../models/office');
var Receipt= require('../models/receipt');
var  Book= require('../models/book');
var  Order= require('../models/order');
var mongoosePaginate= require('mongoose-pagination');


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveOrder (req, res){
    var order = new Order();
    var params= req.body;

    order.reservation_date    = params.reservation_date;
    order.expiration_date     =params.expiration_date;
    order.estado              ='O';// O: Orden || R: reservado
    order.user                =params.user;
    order.receipt             =params.receipt;
    /* receipt.book                =params.book;
    order.office              =params.office; */
    /* order.name                =params.name; */
    order.identification      =params.identification;
    order.pay_date            =params.pay_date;
    order.pin                 =params.pin;
    order.estado_pin          ='N';
    order.total          =params.total;
  //  order.roundsman     =params.roundsman;
    order.roundsman_name=params.roundsman_name;
  //  order.pdf_comprobante=params.pdf_comprobante;
  //  order.pdf_comprobante_date=params.pdf_comprobante_date;
    
  order.destine_direction   =params.destine_direction;
    order.save( (err, OrderStored)=>{
        if(err){   
            res.status(500).send({message: 'Error en el servidor'});        
        }
        else{
                  if(!OrderStored){
                    res.status(404).send({message: 'No se ha guardado la reserva'});        
                }   
                else{
                    res.status(200).send({order: OrderStored});        
                }         
        }
    });
}



/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getOrderById(req, res){
    var orderId= req.params.id;
    console.log("orderId "+orderId);
    Order.findById(orderId,(err,order_details)=>{
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }
        else{
            if(!order_details){
                res.status(404).send({message: 'La orden no existe'});
            }
            else{
                res.status(200).send({order:order_details});
            }
        }
    });
  
}




/***************
 * METODO     : isReceiptEstadoR
 * DESCRIPCION: Devuelve true si existe una reserva en estado 'R'
 * AUTOR      :  Jnieves
 * FECHA      :  25/12/2018
 * *************************** */
function isOrdertEstadoR(req, res){
    console.log(req.params);
    var userId= req.params.id;
    Order.find( {$and:[{user: userId},{estado: 'R'}]}).sort('estado').populate({path: 'user'}).exec(function (err,orderEstado){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!orderEstado){
                      res.status(404).send({message:'El cliente no tiene ninguna reserva'});
                  }else{
                   /*  Receipt.find({estado: 'R'}).exec(function (err, receiptEstado){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptEstado){
                                    res.status(404).send({message:'No hay reservas por falta de pago'});
                                }else{           */
                                    return res.status(200).send({order: orderEstado});
                               /*  }
                        }
                    }); */
                     
                  }
          }
      });
} 


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getOrderByUser(req, res){
    console.log(req.params);
    console.log(req.params.id);
    var UserId= req.params.id;
    if(!UserId){
        res.status(500).send({message: 'Ingrese el usuario'});
    }


    var find=  Order.find({user: UserId}).sort({reservation_date:-1}).populate({path:'user'});


    find.exec(function(err,orderData){
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }
        else{
            if(!orderData){
                res.status(404).send({message: 'No existe la orden'});
            }
            else{
//----------------------------------------------------------------------------------------------------------------------------//
                find.populate({path: 'receipt'}).exec(function (err, receiptOrder){
                    if(err){
                        res.status(500).send({message: 'Error en la petición'});
                    }
                    else{
                        if(!receiptOrder){
                            res.status(404).send({message: 'No existe la orden'});
                        }
                        else{
                            res.status(200).send({order:orderData, order:receiptOrder});
                        }
                    }            
                });
//----------------------------------------------------------------------------------------------------------------------------//
               
            }
        }
    });
  
}



/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getOrderByCedula(req, res){
    console.log(req.params);
    var identificationId= req.params.cedula;

    if(!identificationId){
        return res.status(500).send({message: 'Por favor ingrese la identificación'});
    }
    User.find({identification: identificationId}).exec(function (err, userData){
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }else{
                if(!userData || userData==''){
                    console.log("usuario nuevo");
                    res.status(404).send({message:'Usuario Nuevo'});
                }else{
                    console.log("usuario existente "+userData);
                    var UserId= userData[0]._id;
                    if(!UserId){
                        res.status(500).send({message: 'Ingrese el usuario'});
                    }
                    var busqueda=  Order.find({user: UserId}).sort('estado').populate({path:'user'});
                    busqueda.exec(function(err,orderData){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }
                        else{
                            if(!orderData){
                                res.status(404).send({message: 'No existe la orden'});
                            }
                            else{
                //----------------------------------------------------------------------------------------------------------------------------//
                                    busqueda.populate({path: 'receipt'}).exec(function (err, receiptOrder){
                                    if(err){
                                        res.status(500).send({message: 'Error en la petición'});
                                    }
                                    else{
                                        if(!receiptOrder){
                                            res.status(404).send({message: 'No existe la orden'});
                                        }
                                        else{
                                            res.status(200).send({order:orderData, order:receiptOrder});
                                        }
                                    }            
                                });
                //----------------------------------------------------------------------------------------------------------------------------//
                               
                            }
                        }
                    });
                

                   // return res.status(200).send({user: userData});
                }
        }
    });











    
    
}




/***************
 * METODO     : getOrdertByPAndSeller
 * DESCRIPCION: Obtiene todas las órdenes en donde han sido pagadas y pertenezcan a una oficina especifica
 * AUTOR      :  Jnieves
 * FECHA      :  03/02/2019
 * *************************** */
function getOrdertByPAndSeller(req, res){
    console.log(req.params);
    var officeId= req.params.office;
   // Order.find( {$and:[{office: officeId}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}  ]}).populate({path: 'user'}).exec(function (err,orderEstado){
        Order.find( {$or: [{estado: 'PP'}, {estado: 'PE'}]}).populate({path: 'roundsman'}).populate({path: 'user'}).exec(function (err,orderEstado){
          if(err){
              console.log(err);
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!orderEstado || orderEstado==""){
                      res.status(404).send({message:'El cliente no tiene ninguna reserva'});
                  }else{
                    console.log("orderEstado._id "+orderEstado[0]._id);
                   //  Receipt.find({office: officeId}).exec(function (err, receiptOffice){
                    Receipt.find({$and:[  {$or: [{office: officeId}, {destine_transference: officeId}]}   ,{estado: 'P'},{order: orderEstado[0]._id}]}).exec(function (err, receiptOffice){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptOffice){
                                    res.status(404).send({message:'No hay reservas por falta de pago'});
                                }else{           
                                    return res.status(200).send({order: receiptOffice, order:orderEstado});
                                 }
                        }
                    }); 

                     
                  }
          }
      });
} 





/***************
 * METODO     : getOrderByRundsman
 * DESCRIPCION: Obtiene todas las órdenes en donde han sido pagadas y pertenezcan a una oficina especifica
 * AUTOR      :  Jnieves
 * FECHA      :  03/02/2019
 * *************************** */
function getOrderByRundsman(req, res){
    
    var roundsmanId= req.params.roundsman;
   // Order.find( {$and:[{office: officeId}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}  ]}).populate({path: 'user'}).exec(function (err,orderEstado){
        Order.find(  {$and:[{roundsman: roundsmanId}, {$or: [{$and:[{estado_pin: 'G'},{estado: 'PP'}]}, {$and:[{estado_pin: 'N'},{estado: 'PE'}]}]} ]}).populate({path: 'user'}).exec(function (err,orderroundsman){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!orderroundsman || orderroundsman==""){
                      res.status(404).send({message:'El cliente no tiene ninguna reserva'});
                  }else{
                 /*    Receipt.find({roundsman:roundsmanId} ).exec(function (err, receiptOffice){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptOffice){
                                    res.status(404).send({message:'No hay reservas por falta de pago'});
                                }else{ */           
                                    return res.status(200).send({order:orderroundsman});
                                 }
                      /*   }
                    }); 

                     
                  }*/
          } 
      });
} 

/***************
 * METODO     : getOrderByRundsman
 * DESCRIPCION: Obtiene todas las órdenes en donde han sido pagadas y pertenezcan a una oficina especifica
 * AUTOR      :  Jnieves
 * FECHA      :  03/02/2019
 * *************************** */
function getOrderByRundsmanReport(req, res){
    
    var roundsmanId= req.params.roundsman;
   // Order.find( {$and:[{office: officeId}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}  ]}).populate({path: 'user'}).exec(function (err,orderEstado){
        Order.find(  {$and:[{roundsman: roundsmanId}, {estado_pin: 'E'}]}).populate({path: 'user'}).exec(function (err,orderroundsman){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!orderroundsman ){
                      res.status(404).send({message:'El cliente no tiene ninguna reserva'});
                  }else{
                 /*    Receipt.find({roundsman:roundsmanId} ).exec(function (err, receiptOffice){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptOffice){
                                    res.status(404).send({message:'No hay reservas por falta de pago'});
                                }else{ */           
                                    return res.status(200).send({order:orderroundsman});
                                 }
                      /*   }
                    }); 

                     
                  }*/
          } 
      });
} 




/***************
 * METODO     : getOrderByRundsman
 * DESCRIPCION: Obtiene todas las órdenes en donde han sido pagadas y pertenezcan a una oficina especifica
 * AUTOR      :  Jnieves
 * FECHA      :  03/02/2019
 * *************************** */
function getOrderByRundsman_pdfComprobante(req, res){
    console.log("getOrderByRundsman_pdfComprobante",req.params.roundsman);
    var roundsmanId= req.params.roundsman;
   // Order.find( {$and:[{office: officeId}, {$or: [{estado: 'PP'}, {estado: 'PE'}]},{estado:'E'}  ]}).populate({path: 'user'}).exec(function (err,orderEstado){
        Order.find(  {$and:[{roundsman: roundsmanId}, {$or: [{estado: 'PP'}, {estado: 'PE'}]},{estado_pin:'E'}  ]}).populate({path: 'user'}).exec(function (err,orderroundsman){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!orderroundsman || orderroundsman==""){
                      res.status(404).send({message:'El cliente no tiene ninguna reserva'});
                  }else{
                 /*    Receipt.find({roundsman:roundsmanId} ).exec(function (err, receiptOffice){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptOffice){
                                    res.status(404).send({message:'No hay reservas por falta de pago'});
                                }else{ */           
                                    return res.status(200).send({order:orderroundsman});
                                 }
                      /*   }
                    }); 

                     
                  }*/
          } 
      });
} 




/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function deleteOrder( req, res){
    var orderrId = req.params.id;

    Order.findByIdAndRemove(orderrId,function(err, OrderRemoved){
        if(err){
            res.status(500).send({message: ' Error en el servidor'});
        }else{
                if(!OrderRemoved){
                    res.status(404).send({message: 'La reserva no ha sido eliminado'});
                }else{
                      res.status(200).send({order: OrderRemoved});
               
                }
        }
    });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateOrder(req, res){
    var orderId= req.params.id;
    var update = req.body;
    //Realizamos la actualización de la cantidad  según el id de la Orden
    Order.findByIdAndUpdate(orderId,update, function(err, orderUpdated){
   
    if(err){//Si la petición obtuvo un error el sistema enviará una respuesta 500 con el siguiente mensaje
        res.status(500).send({message: 'Error al realizar la compra'});
    }
    else{//Si la petición fue exitosa 
        if(!orderUpdated){//Si la petición fue exitosa pero no contiene datos  el sistema enviará una respuesta 404 con el siguiente mensaje
            res.status(404).send({message: 'La orden no ha sido actualizada'});
        }
        else{//Si la petición fue exitosa pero no contiene datos  el sistema enviará una respuesta 200 con el siguiente mensaje    
            res.status(200).send({order: orderUpdated});
        }
    } });

}




function  uploadFile(req, res){
    var orderId= req.params.id;
    var filename='No subido...';
    
    if(req.files){
        var file_path= req.files.file.path;
        console.log(file_path);

        var file_split=file_path.split('\\');
      
        var file_name=file_split[1];
        console.log(file_name);
        var exr_explit= file_name.split('\.');
        console.log(exr_explit);
        var file_ext=exr_explit[1];//extención

        if(file_ext =='pdf'){
            Order.findByIdAndUpdate(orderId,{pdf_comprobante:file_name }, (err,orderUpdate)=>{
                if(err){
                    res.status(500).send({message: 'No se ha podido cargar el fichero'});
                }
                else{
                    if(!orderUpdate){
                    res.status(404).send({message: 'No se ha encontrado la orden'});
                    }
                    else{
                       // console.log('Se guardó');
                        res.status(200).send({image:file_name, order: orderUpdate, message:'Dato actualizado correctamente'});
                    }
                }
            });
        }
            else{
                
                console.log('Extención de la imagen no valida');
                res.status(200).send({message: 'Extención de la imagen no valida'});
            }
        }
        else{
            console.log('Subir pdf');
            res.status(200).send({message: 'No has subido ninguna imagen..'});
            
        }
} 

function getFile(req, res){
    console.log("getFile");
     var pdf_file= req.params.pdf_comprobanteFile;
    var path_file = './files/'+pdf_file;
    fs.exists(path_file,function(exists){
        if(exists){
            res.sendFile (path.resolve(path_file));
        }
        else{
            res.status(200).send({message: 'No existe la orden'});        
        }
    }); 
    
}



function getOrderByCedulaPay(req, res){
    console.log(req.params);
    var cedula= req.params.cedula;
   // Order.find( {$and:[{office: officeId}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}  ]}).populate({path: 'user'}).exec(function (err,orderEstado){


User.find( {identification:cedula}).exec(function (err,userData){
    if(err){
        res.status(500).send({message: 'Error en la petición'});
    }else{
            if(!userData || userData==""){
                res.status(404).send({message:'La cédula ingresada no existe'});
            }else{
              console.log("userData[0]._id "+userData[0]._id);
   //-------------------------------------------------------------------------------------------------------------
        Order.find( {$and:[{user: userData[0]._id}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}  ]}).populate({path: 'user'}).exec(function (err,orderEstado){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!orderEstado || orderEstado==""){
                      res.status(404).send({message:'El cliente no tiene orden'});
                  }else{
                    console.log("orderEstado "+orderEstado);
                    return res.status(200).send({ order:orderEstado});
                    
                  /*   Receipt.find({$and:[  {$or: [{office: officeId}, {destine_transference: officeId}]}   ,{estado: 'P'},{order: orderEstado[0]._id}]}).exec(function (err, receiptOffice){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptOffice){
                                    res.status(404).send({message:'No hay reservas por falta de pago'});
                                }else{           
                                    return res.status(200).send({order: receiptOffice, order:orderEstado});
                                 }
                        }
                    });  */

                     
                  }
          }
      });
    }
}
    });

      //-------------------------------------------------------------------------------------------------------------
}



function getOrderByIdPay(req, res){
    console.log(req.params);
    var idOrder= req.params.order;
   // Order.find( {$and:[{office: officeId}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}  ]}).populate({path: 'user'}).exec(function (err,orderEstado){


   //-------------------------------------------------------------------------------------------------------------
        Order.find( {$and:[  {$or: [{estado: 'PP'}, {estado: 'PE'}]},{_id:idOrder}]}).populate({path:'roundsman'}).exec(function (err,orderEstado){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!orderEstado || orderEstado==""){
                      res.status(404).send({message:'El cliente no tiene orden'});
                  }else{
                   console.log("orderEstado",orderEstado[0]);

                    return res.status(200).send({ order:orderEstado[0]});
                   /*  Receipt.find({$and:[  {$or: [{office: officeId}, {destine_transference: officeId}]}   ,{estado: 'P'},{order: orderEstado[0]._id}]}).exec(function (err, receiptOffice){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptOffice){
                                    res.status(404).send({message:'No hay reservas por falta de pago'});
                                }else{           
                                    return res.status(200).send({order: receiptOffice, order:orderEstado});
                                 }
                        }
                    });  */

                     
                  }
          }
      });

      //-------------------------------------------------------------------------------------------------------------
}




function getOrder_filter_my_router_list(req, res){
    console.log(req.body);
    var find=null;
    var idOrder= req.body.order;
    var idCedula=req.body.cedula;
    var conditional=req.body.conditional;
    var idRoundsman= req.body.roundsman;

    //CONTROLAR VALORES NULOS
    if(!idCedula && !idOrder ){
        return res.status(500).send({message: 'Por favor ingrese la información'});
    }
    if(!conditional){// S-> por entregar  , N-> Entregados
        return res.status(500).send({message: 'Error en los datos envíados, por favor verifique lo que desea buscar'});
    }
    if(!idRoundsman){
        return res.status(500).send({message: 'Por favor ingrese la información'});
    }


    
    //PREGUNTAR SI VIENE POR CÉDULA O POR ÓRDEN

    if(idCedula && !idOrder){//VIENE POR CÉDULA

        User.find({identification: idCedula}).exec(function (err, userData){
            if(err){
                res.status(500).send({message: 'Error en la petición'});
            }else{
                    if(!userData || userData==''){
                        console.log("usuario nuevo");
                        res.status(404).send({message:'Usuario Nuevo'});
                    }else{

                        console.log("usuario existente "+userData);
                        var UserId= userData[0]._id;

                        if(!UserId){
                            res.status(500).send({message: 'Ingrese el usuario'});
                        }
           //---------------------------------------------------------------------------------------------------------//             
                        //PREGUNTAR SI VIENE POR RUTAS POR ENTREGAR O ENTREGADAS
                        if(conditional=="S"){//VIENE POR RUTAS POR ENTREGAR
                            find=Order.find( {$and:[ {roundsman: idRoundsman},{user: UserId}, {$or: [{$and: [{estado_pin: 'G'}, {estado: 'PP'}]} ,{$and: [{estado_pin: 'N'}, {estado: 'PE'}]}]}  ]}).populate({path: 'user'}).populate({path:'receipt'});
                        }
                        else{//VIENE POR RUTAS ENTREGADAS
                            find=Order.find( {$and:[ {roundsman: idRoundsman},{user: UserId}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}, {estado_pin: 'E'} ]}).populate({path: 'user'}).populate({path:'receipt'});
                        }
            //---------------------------------------------------------------------------------------------------------//
                        find.exec(function (err,orderroundsman){
                            if(err){
                                res.status(500).send({message: 'Error en la petición'});
                            }else{
                                    if(!orderroundsman || orderroundsman==""){
                                        res.status(404).send({message:'El cliente no tiene ninguna orden'});
                                    }else{
                                        console.log(orderroundsman);
                                          return res.status(200).send({order:orderroundsman});
                                        }

                                } 
                        });

                    }
                }
            });

    }
    else{//VIENE POR ÓRDEN
        //---------------------------------------------------------------------------------------------------------//             
        //PREGUNTAR SI VIENE POR RUTAS POR ENTREGAR O ENTREGADAS

        if(conditional=="S"){//VIENE POR RUTAS POR ENTREGAR
            find=Order.find( {$and:[ {roundsman: idRoundsman},{_id: idOrder}, {$or: [{$and: [{estado_pin: 'G'}, {estado: 'PP'}]} ,{$and: [{estado_pin: 'N'}, {estado: 'PE'}]}]}   ]}).populate({path: 'user'}).populate({path:'receipt'}); 
        }
        else{//VIENE POR RUTAS ENTREGADAS
            find=Order.find( {$and:[ {roundsman: idRoundsman},{_id: idOrder}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}, {estado_pin: 'E'} ]}).populate({path: 'user'}).populate({path:'receipt'});
        }
        //---------------------------------------------------------------------------------------------------------//             
        find.exec(function (err,orderroundsman){
            if(err){
                res.status(500).send({message: 'Error en la petición'});
            }else{
                    if(!orderroundsman || orderroundsman==""){
                        if(conditional=="S"){//VIENE POR RUTAS POR ENTREGAR
                            res.status(404).send({message:'La órden no se encuentra en ése estado'});
                        }
                        else{//VIENE POR RUTAS ENTREGADAS
                            res.status(404).send({message:'La órden no se encuentra entregada'});
                        }

                        
                    }else{
                      
                          return res.status(200).send({order:orderroundsman[0]});
                        }

                } 
        });


    }


    

    /**********************************    RUTAS POR ENTREGAR   **************************** *****************************/
    //ROLL_ROUNDSMAN , RUTAS POR ENTREGAR , BÚSQUEDA POR NÚMERO DE ORDEN
//    find=Order.find( {$and:[ {roundsman: idRoundsman},{_id: idOrder}, {$or: [{estado_pin: 'G'}, {estado: 'PP'}]}, {$or: [{estado_pin: 'N'}, {estado: 'PE'}]}  ]}).populate({path: 'user'}).populate({path:'receipt'}); 

    //ROLL_ROUNDSMAN , RUTAS POR ENTREGAR , BÚSQUEDA POR NÚMERO DE CÉDULA
//    find=Order.find( {$and:[ {roundsman: idRoundsman},{user: idUser}, {$or: [{estado_pin: 'G'}, {estado: 'PP'}]}, {$or: [{estado_pin: 'N'}, {estado: 'PE'}]}  ]}).populate({path: 'user'}).populate({path:'receipt'});


    /************************************   RUTAS ENTREGADAS  ************************** *****************************/
    //ROLL_ROUNDSMAN , RUTAS ENTREGADAS , BÚSQUEDA POR NÚMERO DE ORDEN
//    find=Order.find( {$and:[ {roundsman: idRoundsman},{_id: idOrder}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}, {estado_pin: 'E'} ]}).populate({path: 'user'}).populate({path:'receipt'});

    //ROLL_ROUNDSMAN , RUTAS ENTREGADAS , BÚSQUEDA POR NÚMERO DE CÉDULA
//    find=Order.find( {$and:[ {roundsman: idRoundsman},{user: idUser}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}, {estado_pin: 'E'} ]}).populate({path: 'user'}).populate({path:'receipt'});

/*     find.exec(function (err,orderroundsman){
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }else{
                if(!orderroundsman || orderroundsman==""){
                    res.status(404).send({message:'El cliente no tiene ninguna reserva'});
                }else{
                     
                                return res.status(200).send({order:orderroundsman});
                            }
                } 
    }); */



}


module.exports={
saveOrder,
getOrderById,
getOrderByUser,
getOrdertByPAndSeller,
getOrderByRundsman,
getOrderByRundsmanReport,
//getOrderBySelle,
deleteOrder,
updateOrder,
isOrdertEstadoR,
getOrderByCedula,

uploadFile,
getFile,
getOrderByRundsman_pdfComprobante,

getOrderByCedulaPay,
getOrderByIdPay,


getOrder_filter_my_router_list



}