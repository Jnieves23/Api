'use strict'
var fs = require('fs');
var path = require('path');

var Schedule= require('../models/schedule');

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getSchedule(req, res){
    var scheduleId= req.params.id;
    Schedule.findById(scheduleId,(err,schedule)=>{
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }
        else{
            if(!schedule){
                res.status(404).send({message: 'El horario no existe'});
            }
            else{
                res.status(200).send({schedule:schedule});
            }
        }
    });
  
}

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveSchedule(req,res){
    var schedule = new Schedule();

    var params = req.body;
    schedule.name= params.name;
    schedule.monday= params.monday;
    schedule.tuesday= params.tuesday;
    schedule.wednesday= params.wednesday;
    schedule.thursday= params.thursday;
    schedule.friday= params.friday;
    schedule.saturday= params.saturday;
    schedule.sunday= params.sunday;
    schedule.estado='S';

    schedule.save((err,scheduleStored)=>{
        if(err){
            req.status(500).send({message:'Error al guardar el horario'});
        }
        else{
            if(!scheduleStored){
                req.status(404).send({message: 'El horario no ha sido guardado'});
            }
            else{
                res.status(200).send({schedule: scheduleStored});
            }
        }
    });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getSchedules(req, res){

    var page = req.params.page || 1;
    var itemsPerPage = Number(req.params.itemsperpage) || 3;
    var sort = req.params.sort || 'name';
    


    Schedule.find().sort(sort).paginate( page,itemsPerPage, function (err, schedules,total){
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }else{
                if(!schedules){
                    res.status(404).send({message:'No hay horarios'});
                }else{
                    return res.status(200).send({total_items: total, schedules:  schedules});
                }
        }
    });

}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getSchedules_sin_page(req, res){

    console.log("req.params.estado",req.params.estado);
    var estadoId=req.params.estado;
        if(req.params.estado!='null'){
            var find=Schedule.find({estado:estadoId});
            console.log("not null");
        }else{
            var find=Schedule.find({});
            console.log("ds");
        }

        find.exec( function (err, schedules){
            if(err){
                res.status(500).send({message: 'Error en la petición'});
            }else{
                    if(!schedules){
                        res.status(404).send({message:'No hay horarios'});
                    }else{
                        return res.status(200).send({ schedules:  schedules});
                    }
            }
        });

}
/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateSchedule(req, res){
    var scheduleId= req.params.id;
    var update = req.body;
//findOneAndUpdate
    Schedule.findByIdAndUpdate(scheduleId,update, function(err, scheduleUpdated){
   
    if(err){
        res.status(500).send({message: 'Error al guardar el horario'});
    }
    else{
        if(!scheduleUpdated){
            res.status(404).send({message: 'El horario no ha sido actualizado'});
        }
        else{
            res.status(200).send({schedule: scheduleUpdated});
        }
    } });

}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function deleteSchedule( req, res){
    var scheduleId = req.params.id;

    Schedule.findByIdAndRemove(scheduleId,function(err, scheduleIdRemoved){
        if(err){
            res.status(500).send({message: ' Error al eliminar el horario'});
        }else{
                if(!scheduleIdRemoved){
                    res.status(404).send({message: 'El horario no ha sido eliminado'});
                }else{
                
                                res.status(200).send({schedule: scheduleIdRemoved});
                       
                }
        }
    });
}


module.exports={
    getSchedule,
    saveSchedule,
    getSchedules,
    getSchedules_sin_page,
    updateSchedule,
    deleteSchedule,
}