'use strict'
var fs = require('fs');
var path = require('path');

var Office= require('../models/office');
var Company= require('../models/company');
var Quota= require('../models/quota_book_office');
var Book= require('../models/book');
var mongoosePaginate= require('mongoose-pagination');

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getOffice(req, res){
    var officeId= req.params.id;
    Office.findById(officeId).populate({path: 'Company'}).exec(function( err,office){
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }
        else{
            if(!office){
                res.status(404).send({message: 'La sucursal no existe'});
            }
            else{
                res.status(200).send({ office:office});
            }
        }
    });
  }
 
/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveOffice (req, res){
    var office = new Office();

    var params= req.body;
    office.name = params.name;
    office.description= params.description;
    office.addres = params.addres;
    office.image = 'null';
    office.phone=params.phone;
  //  office.schedule =params.schedule;
    office.company ='5c088bc79173524074bca759';
    office.lat  =params.lat;
    office.lng  = params.lng;
    office.estado='S';
  


    office.save( (err, OfficeStored)=>{
        if(err){   
            res.status(500).send({message: 'Error en el servidor'}); 
            console.log(err);       
        }
        else{
                  if(!OfficeStored){
                    res.status(404).send({message: 'No se ha guardado la sucursal'});        
                }   
                else{
                    res.status(200).send({office: OfficeStored});        
                }         
        }
    });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getOffices(req, res){

    var officeId= req.params.company;//empresa
    var scheduleId= req.params.schedule;//horario
    console.log(req.params.page);

    if(! scheduleId || req.params.page=='null' ){
        console.log("getOffices");
        var find= Office.find({estado:'S'}).sort('name');
    }
    else{
        var find= Office.find({$and:[{schedule: scheduleId},{estado:'S' } ]}).sort('name').populate({path: 'schedule'});
    }

     
    find.exec(function (err, offices,total){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!offices){
                      res.status(404).send({message:'No hay sucursales'});
                  }else{

                    
                      return res.status(200).send({offices: offices});
                  }
          }
      });
  
  }
  




  
/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getOfficesP(req, res){
  
      var page = req.params.page || 1;
      var itemsPerPage = Number(req.params.itemsperpage) || 4;
      var sort = req.params.sort || 'name';
      
  
  
      Office.find().sort(sort).paginate( page,itemsPerPage,  (err, offices,total)=>{
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!offices){
                      res.status(404).send({message:'No hay sucursales'});
                  }else{
                      return res.status(200).send({total_items: total, offices:  offices});
                  }
          }
      });
  
  }


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateOffice(req, res){
    var officeId= req.params.id;
    var update = req.body;

    Office.findByIdAndUpdate(officeId,update, function(err, officeUpdated){
   
    if(err){
        res.status(500).send({message: 'Error al guardar la sucursal'});
    }
    else{
        if(!officeUpdated){
            res.status(404).send({message: 'La Sucursal no ha sido actualizada'});
        }
        else{
            res.status(200).send({office: officeUpdated});
        }
    } });

}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function deleteOffice( req, res){
    var officeId = req.params.id;
    Quota.find({office: officeId}).remove(function(err, quotaRemoved){
        if(err){
            res.status(500).send({message: ' Error al guardar la cuota'});
        }else{
                if(!quotaRemoved){
                    res.status(404).send({message: 'La cuota no ha sido eliminado'});
                }else{
                    //res.status(200).send({quotaRemoved});
/*                     Book.find({quota: quotaRemoved._id}).remove(function(err, bookRemoved){
                        if(err){
                            res.status(500).send({message:'Error al eliminar la cuota'});
                        }
                        else{
                            if(!bookRemoved){
                                res.status(404).send({message:'La cuota no ha sido eliminado'});
                            }
                            else{
                               // res.status(200).send({quota: quotaRemoved});
                             
                            }
                        }
                    }); */
                    Office.findByIdAndRemove(officeId,function(err, officekRemoved){
                        if(err){
                            res.status(500).send({message:'Error al eliminar la sucursal'});
                        }
                        else{
                            if(!officekRemoved){
                                res.status(404).send({message:'La sucursal ha sido eliminada'});
                            }
                            else{
                                res.status(200).send({office: officekRemoved});
                            }
                        }
                    });
                  
                }
        }
    });
           
}


/***************
 * METODO     : uploadImage
 * DESCRIPCION: 
 * AUTOR      : Jnieves
 * FECHA      : 08/11/2018
 * *************************** */
function uploadImage(req,res){
    var officeId= req.params.id;
    var filename='No subido...';

    if(req.files){
        var file_path= req.files.image.path;
        console.log(file_path);
        var file_split=file_path.split('\\');
        var file_name=file_split[2];
        var exr_explit= file_name.split('\.');
        var file_ext=exr_explit[1];//extención

        if(file_ext =='png' || file_ext=='jpg'|| file_ext =='gif'){
            Office.findByIdAndUpdate(officeId,{image:file_name }, (err,officeUpdate)=>{
                if(err){
                    res.status(500).send({message: 'No se ha podido cargar el fichero'});
                }
                else{
                    if(!officeUpdate){
                    res.status(404).send({message: 'No se ha encontrado la sucursal'});
                    }
                    else{
                        res.status(200).send({office: officeUpdate});
                    }
                }
            });
        }
        else{
            res.status(200).send({message: 'Extención de la imagen no valida'});
        }
    }
    else{
        res.status(200).send({message: 'No has subido ninguna imagen..'});
    }
}


/***************
 * METODO     : getImageFile
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function getImageFile(req, res){
    var image_file= req.params.imageFile;
    var path_file = './uploads/offices/'+image_file;
    fs.exists(path_file,function(exists){
        if(exists){
            res.sendFile (path.resolve(path_file));
        }
        else{
            res.status(200).send({message: 'No existe la imagen'});        
        }
    });
}




module.exports={
    
    saveOffice,
    getOffices,
    getOfficesP,
    getOffice,
    updateOffice  ,
    deleteOffice,
    getImageFile,
    uploadImage,


}