'use strict'
var fs = require('fs');
var path = require('path');

var Author= require('../models/author');
var  Book= require('../models/book');
var mongoosePaginate= require('mongoose-pagination');

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getAuthor(req, res){
    var authorId= req.params.id;
    console.log("authorId",authorId);
    Author.findById(authorId,(err,author)=>{
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }
        else{
            if(!author){
                res.status(404).send({message: 'El autor no existe'});
            }
            else{
                res.status(200).send({ author:author});
            }
        }
    });
  
}

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveAuthor(req,res){
    var author = new Author();

    var params = req.body;
    author.name= params.name;
    author.description= params.description;
    author.image= 'null';
    author.estado='S';

    author.save((err,authorStored)=>{
        if(err){
            req.status(500).send({message:'Error al guardar el autor'});
        }
        else{
            if(!authorStored){
                req.status(404).send({message: 'El autor no ha sido guardado'});
            }
            else{
                res.status(200).send({author: authorStored});
            }
        }
    });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getAuthors(req, res){
  /*   if(req.params.page){
        var page = req.params.page;    
    }
    else{
        var page = 1;
    }
        var itemsPerPage= 3;
 */

    var page = req.params.page || 1;
    var itemsPerPage = Number(req.params.itemsperpage) || 4;
    var sort = req.params.sort || 'name';
    


    Author.find(/* {estado:'S' } */).sort(sort).paginate( page,itemsPerPage, function (err, authors,total){
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }else{
                if(!authors){
                    res.status(404).send({message:'No hay autores'});
                }else{
                    return res.status(200).send({total_items: total, authors:  authors});
                }
        }
    });

}

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getAuthorsDeshabilitados(req, res){
    /*   if(req.params.page){
          var page = req.params.page;    
      }
      else{
          var page = 1;
      }
          var itemsPerPage= 3;
   */
  
      var page = req.params.page || 1;
      var itemsPerPage = Number(req.params.itemsperpage) || 4;
      var sort = req.params.sort || 'name';
      
  
  
      Author.find({estado:'N' }).sort(sort).paginate( page,itemsPerPage, function (err, authors,total){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!authors){
                      res.status(404).send({message:'No hay autores'});
                  }else{
                      return res.status(200).send({total_items: total, authors:  authors});
                  }
          }
      });
  
  }





/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getAuthorsSinPage(req, res){
    console.log("req.params.estado",req.params.estado);
    var estadoId=req.params.estado;
        if(req.params.estado!='null'){
            var find=Author.find({estado:estadoId});
            console.log("not null");
        }else{
            var find=Author.find({});
            console.log("ds");
        }
      find.exec( function (err, authors){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!authors){
                      res.status(404).send({message:'No hay autores'});
                  }else{
                      console.log(authors);
                      return res.status(200).send({ authors:  authors});
                  }
          }
      });
  
  }




/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateAuthor(req, res){
    var authorId= req.params.id;
    var update = req.body;
    console.log("authorId, ",authorId);
//findOneAndUpdate
    Author.findByIdAndUpdate(authorId,update, function(err, authorUpdated){
   
    if(err){
        res.status(500).send({message: 'Error al guardar el autor'});
    }
    else{
        if(!authorUpdated){
            res.status(404).send({message: 'El autor no ha sido actualizado'});
        }
        else{
            res.status(200).send({author: authorUpdated});
        }
    } });

}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function deleteAuthor( req, res){
    var authorId = req.params.id;

    Author.findByIdAndRemove(authorId,function(err, authorRemoved){
        if(err){
            res.status(500).send({message: ' Error al guardar el autor'});
        }else{
                if(!authorRemoved){
                    res.status(404).send({message: 'El autor no ha sido eliminado'});
                }else{
                    //res.status(200).send({authorRemoved});
                    Book.find({author: authorRemoved._id}).remove(function(err, bookRemoved){
                        if(err){
                            res.status(500).send({message:'Error al eliminar el libro'});
                        }
                        else{
                            if(!bookRemoved){
                                res.status(404).send({message:'El libro no ha sido eliminado'});
                            }
                            else{
                                res.status(200).send({author: authorRemoved});
                            }
                        }
                    });
                }
        }
    });
}


/***************
 * METODO     : uploadImage
 * DESCRIPCION: 
 * AUTOR      : Jnieves
 * FECHA      : 08/11/2018
 * *************************** */
function uploadImage(req,res){
    var authorId= req.params.id;
    var filename='No subido...';

    if(req.files){
        var file_path= req.files.image.path;
        console.log(file_path);
        var file_split=file_path.split('\\');
        var file_name=file_split[2];
        var exr_explit= file_name.split('\.');
        var file_ext=exr_explit[1];//extención

        if(file_ext =='png' || file_ext=='jpg'|| file_ext =='gif'){
            Author.findByIdAndUpdate(authorId,{image:file_name }, (err,authorUpdate)=>{
                if(err){
                    res.status(500).send({message: 'No se ha podido cargar el fichero'});
                }
                else{
                    if(!authorUpdate){
                    res.status(404).send({message: 'No se ha encontrado el autor'});
                    }
                    else{
                        res.status(200).send({authorUpdate});
                    }
                }
            });
        }
        else{
            res.status(200).send({message: 'Extención de la imagen no valida'});
        }
    }
    else{
        res.status(200).send({message: 'No has subido ninguna imagen..'});
    }
}


/***************
 * METODO     : getImageFile
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function getImageFile(req, res){
    var image_file= req.params.imageFile;
    var path_file = './uploads/authors/'+image_file;
    fs.exists(path_file,function(exists){
        if(exists){
            res.sendFile (path.resolve(path_file));
        }
        else{
            res.status(200).send({message: 'No existe la imagen'});        
        }
    });
}





module.exports={
    getAuthor,
    saveAuthor,
    getAuthors,
    getAuthorsDeshabilitados,
    updateAuthor,
    deleteAuthor,
    uploadImage,
    getImageFile,
    getAuthorsSinPage,
    
    

}