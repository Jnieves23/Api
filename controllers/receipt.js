'use strict'
var  Receipt= require('../models/receipt');
var  User= require('../models/user');
var  Book= require('../models/book');
var  Office= require('../models/office');
var  Quota= require('../models/quota_book_office');
var  mongoosePaginate= require('mongoose-pagination');

/***************
 * METODO     : getReceipt
 * DESCRIPCION: Devuelve todas las reservas  de un usuario.
 * AUTOR      :  Jnieves
 * FECHA      :  02/12/2018
 * *************************** */
function getReceiptByUser(req, res){
    console.log("req.params "+req.params.seller);

    var userId= req.params.id;
    var sellerId= req.params.seller;
    var officeId= req.params.office;

    if(officeId){
        console.log("officeId "+officeId);
        var find= Receipt.find({office:officeId}).populate({path: 'office'});
        console.log(find);
    }
    if(userId){
        console.log("userId "+userId);
        var find= Receipt.find({user: userId}).sort('estado').populate({path: 'user'});
    }
    if(sellerId){
        console.log("sellerId "+sellerId);
        var find= Receipt.find({user: sellerId}).sort('estado').populate({path: 'user'}).populate({path:'quota'}).populate({path:'order'});
    }
    find.exec(function (err, receipt){ 
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!receipt){
                      res.status(404).send({message:'No hay libros'});
                  }else{
                    find.populate({path: 'book'}).exec(function (err, receiptBook){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptBook){
                                    res.status(404).send({message:'No hay libros'});
                                }else{          

                                    //return res.status(200).send({receipt: receipt, receipt:receiptBook});
                                    find.populate({path: 'user'}).exec(function (err, receiptuser){
                                        if(err){
                                            res.status(500).send({message: 'Error en la petición'});
                                        }else{
                                                if(!receiptuser){
                                                    res.status(404).send({message:'No hay usurios'});
                                                }else{          
                                       //             return res.status(200).send({receipt: receipt, receipt:receiptBook, receipt:receiptuser});
                                                        find.populate({path: 'office'}).exec(function (err, receiptoffice){
                                                            if(err){
                                                                res.status(500).send({message: 'Error en la petición'});
                                                            }else{
                                                                    if(!receiptoffice){
                                                                        res.status(404).send({message:'No hay usurios'});
                                                                    }else{          
                                                                        return res.status(200).send({receipt: receipt, receipt:receiptBook, receipt:receiptuser, receipt:receiptoffice});
                                                                    }
                                                            }
                                                        });
                                                }
                                        }
                                    });
                                }
                        }
                    });
                  }
          }
      });
}



/***************
 * METODO     : getReceipt
 * DESCRIPCION: Devuelve todas las reservas  de un usuario.
 * AUTOR      :  Jnieves
 * FECHA      :  02/12/2018
 * *************************** */
function getReceiptBySellerReport(req, res){

    var sellerId= req.params.seller;


    if(sellerId){
       
        var find= Receipt.find( {$and:[{user: sellerId}, {estado:'P'}  ]}).sort('estado').populate({path: 'user'}).populate({path:'quota'}).populate({path:'order'});
    }
    find.exec(function (err, receipt){ 
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!receipt){
                      res.status(404).send({message:'No hay libros'});
                  }else{
                    find.populate({path: 'book'}).exec(function (err, receiptBook){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptBook){
                                    res.status(404).send({message:'No hay libros'});
                                }else{          

                                    //return res.status(200).send({receipt: receipt, receipt:receiptBook});
                                    find.populate({path: 'user'}).exec(function (err, receiptuser){
                                        if(err){
                                            res.status(500).send({message: 'Error en la petición'});
                                        }else{
                                                if(!receiptuser){
                                                    res.status(404).send({message:'No hay usurios'});
                                                }else{          
                                       //             return res.status(200).send({receipt: receipt, receipt:receiptBook, receipt:receiptuser});
                                                        find.populate({path: 'office'}).exec(function (err, receiptoffice){
                                                            if(err){
                                                                res.status(500).send({message: 'Error en la petición'});
                                                            }else{
                                                                    if(!receiptoffice){
                                                                        res.status(404).send({message:'No hay usurios'});
                                                                    }else{          
                                                                        return res.status(200).send({receipt: receipt, receipt:receiptBook, receipt:receiptuser, receipt:receiptoffice});
                                                                    }
                                                            }
                                                        });
                                                }
                                        }
                                    });
                                }
                        }
                    });
                  }
          }
      });
}



/***************
 * METODO     : getReceipt
 * DESCRIPCION: Devuelve todas las reservas  de un usuario.
 * AUTOR      :  Jnieves
 * FECHA      :  02/12/2018
 * *************************** */
function getReceiptByOfficeReport(req, res){

    var officeId= req.params.office;


    if(officeId){
       
        var find= Receipt.find( {$and:[{office: officeId}, {estado:'P'}  ]}).sort('estado').populate({path: 'user'}).populate({path:'quota'}).populate({path:'order'}).populate({path:'office'});
    }
    find.exec(function (err, receipt){ 
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!receipt){
                      res.status(404).send({message:'No hay libros'});
                  }else{
                    find.populate({path: 'book'}).exec(function (err, receiptBook){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptBook){
                                    res.status(404).send({message:'No hay libros'});
                                }else{          

                                    //return res.status(200).send({receipt: receipt, receipt:receiptBook});
                                    find.populate({path: 'user'}).exec(function (err, receiptuser){
                                        if(err){
                                            res.status(500).send({message: 'Error en la petición'});
                                        }else{
                                                if(!receiptuser){
                                                    res.status(404).send({message:'No hay usurios'});
                                                }else{          
                                       //             return res.status(200).send({receipt: receipt, receipt:receiptBook, receipt:receiptuser});
                                                        find.populate({path: 'office'}).exec(function (err, receiptoffice){
                                                            if(err){
                                                                res.status(500).send({message: 'Error en la petición'});
                                                            }else{
                                                                    if(!receiptoffice){
                                                                        res.status(404).send({message:'No hay usurios'});
                                                                    }else{          
                                                                        return res.status(200).send({receipt: receipt, receipt:receiptBook, receipt:receiptuser, receipt:receiptoffice});
                                                                    }
                                                            }
                                                        });
                                                }
                                        }
                                    });
                                }
                        }
                    });
                  }
          }
      });
}


/***************
 * METODO     : getReceipt
 * DESCRIPCION: Devuelve todas las reservas  de un usuario.
 * AUTOR      :  Jnieves
 * FECHA      :  02/12/2018
 * *************************** */
function getReceiptByOffice_Transference_Report(req, res){

    var officeId= req.params.office;


    if(officeId){
       
        var find= Receipt.find( {$and:[{destine_transference: officeId}, {estado:'P'}  ]}).sort('estado').populate({path: 'user'}).populate({path:'quota'}).populate({path:'order'}).populate({path:'office'});
    }
    find.exec(function (err, receipt){ 
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!receipt){
                      res.status(404).send({message:'No hay libros'});
                  }else{
                    find.populate({path: 'book'}).exec(function (err, receiptBook){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptBook){
                                    res.status(404).send({message:'No hay libros'});
                                }else{          

                                    //return res.status(200).send({receipt: receipt, receipt:receiptBook});
                                    find.populate({path: 'user'}).exec(function (err, receiptuser){
                                        if(err){
                                            res.status(500).send({message: 'Error en la petición'});
                                        }else{
                                                if(!receiptuser){
                                                    res.status(404).send({message:'No hay usurios'});
                                                }else{          
                                       //             return res.status(200).send({receipt: receipt, receipt:receiptBook, receipt:receiptuser});
                                                        find.populate({path: 'office'}).exec(function (err, receiptoffice){
                                                            if(err){
                                                                res.status(500).send({message: 'Error en la petición'});
                                                            }else{
                                                                    if(!receiptoffice){
                                                                        res.status(404).send({message:'No hay usurios'});
                                                                    }else{          
                                                                        return res.status(200).send({receipt: receipt, receipt:receiptBook, receipt:receiptuser, receipt:receiptoffice});
                                                                    }
                                                            }
                                                        });
                                                }
                                        }
                                    });
                                }
                        }
                    });
                  }
          }
      });
}



/***************
 * METODO     : getReceiptByBook_Report
 * DESCRIPCION: Devuelve todas las reservas  de un usuario.
 * AUTOR      :  Jnieves
 * FECHA      :  02/12/2018
 * *************************** */
function getReceiptByBook_Report(req, res){

    
    var bookId= req.params.book;//Asignamos en una variable el parámetro libro enviado por el administrador
    if(bookId){//Si la información ingresada es diferente de NULL se realiza la siguiente consulta
        var find= Receipt.find( {$and:[{book: bookId}  ]}).populate({path: 'user'}).populate({path:'quota'}).populate({path:'order'}).populate({path:'office'}).populate({path:'book'});
    }else{//Si la información ingresada es  NULL (vacía) el sistema enviará una respuesta 500 con el siguiente mensaje
        return res.status(500).send({message: 'Se requiere la información para realizar la consulta, por favor ingrese el libro'});
    }
    find.exec(function (err, receipt){ 
          if(err){//Si la petición obtuvo un error el sistema enviará una respuesta 500 con el siguiente mensaje
              res.status(500).send({message: 'Error en la petición'});
          }else{//Si la petición fue exitosa 
                if(!receipt){//Si la petición fue exitosa pero no contiene datos  el sistema enviará una respuesta 404 con el siguiente mensaje
                    res.status(404).send({message:'No hay libros'});
                }else{ //Si la petición fue exitosa pero no contiene datos  el sistema enviará una respuesta 200 con el siguiente mensaje    
                        return res.status(200).send({receipt: receipt});
                    }
                }
         
      });
}





/***************
 * METODO     : getReceiptByOffice
 * DESCRIPCION: Devuelve todas las reservas compradas de una sucursal.
 * AUTOR      :  Jnieves
 * FECHA      :  25/12/2018
 * *************************** */
function getReceiptByOffice(req, res){
    console.log(req.params);
    var userId= req.params.id;
    
   /*  if(sucursal){
        var find= Receipt.find({}).sort('book');
    } */
    if(userId){
        var find= Book.find({user: userId}).sort('estado');
    }
    find.populate({path: 'user'}).exec(function (err, receipt){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!receipt){
                      res.status(404).send({message:'No hay libros'});
                  }else{
                    find.populate({path: 'book'}).exec(function (err, receiptBook){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptBook){
                                    res.status(404).send({message:'No hay libros'});
                                }else{          
                                    return res.status(200).send({receipt: receipt, receipt:receiptBook});
                                }
                        }
                    });
                     
                  }
          }
      });
}






/***************
 * METODO     : getReceiptById
 * DESCRIPCION: Devuelve la reserva según su ID
 * AUTOR      :  Jnieves
 * FECHA      :  25/12/2018
 * *************************** */
 function getReceiptById(req, res){
    var receiptId= req.params.id;
    var find=Receipt.findById(receiptId).populate({path: 'user'});
    find.exec(function( err,receiptUser){
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }
        else{
            if(!receiptUser){
                res.status(404).send({message: 'El recibo no existe '});
            }
            else{
                find.populate({path: 'book'}).exec(function( err,receiptBook){   
                    if(err){
                        res.status(500).send({message: 'Error en la petición'});
                    }
                    else{
                        if(!receiptBook){
                            res.status(404).send({message: 'El recibo no existe por el libro'});
                        }
                        else{

                            //res.status(200).send({receipt: receiptUser, receipt:receiptBook });
                            find.populate({path: 'office'}).exec(function( err,receiptOffice){   
                                if(err){
                                    res.status(500).send({message: 'Error en la petición'});
                                }
                                else{
                                    if(!receiptOffice){
                                        res.status(404).send({message: 'El recibo no existe por la oficina'});
                                    }
                                    else{
                                        res.status(200).send({receipt: receiptUser, receipt:receiptBook, receipt: receiptOffice });
                                    }   
                                }
                            });


                        }   
                    }
                });
            }
            } 
    });  
} 


/***************
 * METODO     : getReceiptByOrder
 * DESCRIPCION: Devuelve todas las reservas  de un usuario.
 * AUTOR      :  Jnieves
 * FECHA      :  31/01/2019
 * *************************** */
function getReceiptByOrder(req, res){
    console.log("req.params "+req.params.order);
    var orderId= req.params.order;

    if(orderId){
        console.log("orderId "+orderId);
        var find= Receipt.find({order:orderId}).populate({path: 'book'}).populate({path: 'user'}).populate({path: 'office'}).populate({path: 'roundsman'});
       
    }
    find.exec(function (err, receipt){ 
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!receipt){
                      res.status(404).send({message:'No hay libros'});
                  }else{      
                        console.log("receipt ",receipt);
                        return res.status(200).send({receipt: receipt});
                    }
                }
      });
}


/***************
 * METODO     : isReceiptEstadoR
 * DESCRIPCION: Devuelve true si existe una reserva en estado 'R'
 * AUTOR      :  Jnieves
 * FECHA      :  25/12/2018
 * *************************** */
function isReceiptEstadoR(req, res){
    console.log(req.params);
    var userId= req.params.id;
    Receipt.find( {$and:[{user: userId},{estado: 'R'}]}).sort('estado').populate({path: 'user'}).exec(function (err, receiptEstado){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!receiptEstado){
                      res.status(404).send({message:'El cliente no tiene ninguna reserva'});
                  }else{
                   /*  Receipt.find({estado: 'R'}).exec(function (err, receiptEstado){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptEstado){
                                    res.status(404).send({message:'No hay reservas por falta de pago'});
                                }else{           */
                                    return res.status(200).send({receipt: receiptEstado});
                               /*  }
                        }
                    }); */
                     
                  }
          }
      });
} 




/***************
 * METODO     :  saveReceipt
 * DESCRIPCION:
 * AUTOR      :   Jnieves
 * FECHA      :  02/12/2018
 * *************************** */
function saveReceipt (req, res){
    var receipt = new Receipt();//Se instancia un objeto de tipo Reserva

    var params= req.body;

    console.log(params);
    //Se asigna por cada campo los datos enviados por el cliente/vendedor

    receipt.estado              ='R';/*Cuando se realiza una reserva por defecto el estado será "R". Los estados que posee esta variable son los siguientes( P: pagado || R: reservado)*/
    receipt.user                =params.user; 
    receipt.book                =params.book;
    receipt.office              =params.office;
    receipt.name                =params.name;
    receipt.price               =params.price;
    receipt.cantidad            =params.cantidad;
    receipt.transference        =false;/* Variable que permite conocer si la reserva requiere de una transferencia en caso de que se retire en una sucursal que no se encuentra aquellos ejemplares*/
    receipt.destine_transference= params.destine_transference;//Campo que permite conocer cual es el destino del libro que requiere la transferencia
    receipt.roundsman_name      = params.roundsman_name;
    receipt.quota               = params.quota;
    receipt.receipt_date        =params.receipt_date; //Campo que permite almacenar la fecha que se realiza la reserva
    receipt.receipt_expiration_date=params.receipt_expiration_date; //Campo que permite almacenar la fecha que se realiza la reserva
    //receipt.destine_direction   =params.destine_direction;
    receipt.save( (err, ReceiptStored)=>{//Se realiza el almacenamiento de la reserva

        if(err){   //Si la petición obtuvo un error se enviará una respuesta 500 con el siguiente mensaje 
            console.log(err);
            res.status(500).send({message: 'Error en el servidor'});        
        }
        else{//Si la petición fue exitosa 
                  if(!ReceiptStored){//Pregunto si la respuesta obtenida de la petición fue realizada pero no contiene información, el sistema enviará una respuesta 404 con el siguiente mensaje
                    res.status(404).send({message: 'No se ha guardado el recibo'});        
                }   
                else{//Si la respuesta obtenida de la petición fue realizada y muestra contenido , el sistema enviará una respuesta 200 con el Objeto JSON 
                    res.status(200).send({receipt: ReceiptStored});        
                }         
        }
    });
}

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateReceipt(req, res){
    var receiptId= req.params.id;
    var update = req.body;

    Receipt.findByIdAndUpdate(receiptId,update, function(err, receiptUpdated){
   
    if(err){
        res.status(500).send({message: 'Error en la actualización de la reserva'});
    }
    else{
        if(!receiptUpdated){
            res.status(404).send({message: 'La reserva no ha sido actualizada'});
        }
        else{
            res.status(200).send({receipt: receiptUpdated});
        }
    } });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function deleteReceipt( req, res){
    console.log(req.params);
    var receiptId = req.params.id;
    //Realizamos la eliminación de la reserva  según el id enviada por el cliente/vendedor por parámetro
    Receipt.findByIdAndRemove(receiptId,function(err, receiptRemoved){
        if(err){//Si la petición obtuvo un error el sistema enviará una respuesta 500 con el siguiente mensaje
            res.status(500).send({message:'Error al eliminar la reserva'});
        }
        else{//Si la petición fue exitosa 
            if(!receiptRemoved){//Si la petición fue exitosa pero no contiene datos  el sistema enviará una respuesta 404 con el siguiente mensaje
                res.status(404).send({message:'La reserva ha sido eliminada'});
            }
            else{//Si la petición fue exitosa pero no contiene datos  el sistema enviará una respuesta 200 con el siguiente mensaje    
                res.status(200).send({receipt: receiptRemoved});
            }
        }
    });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function deleteReceiptWithId( req, res){
    console.log("DELETE RECEIPT WITH ID");
    console.log(req.params);
    var receiptId = req.params.id;

//LLAMAR AL MÉTODO GET PARA OBTENER LA INFORMACIÓN DE ESA RESERVA

    Receipt.findById(receiptId).populate({path: 'book'}).exec(function( err,receiptBook){
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }
        else{
            if(!receiptBook){
                res.status(404).send({message: 'El recibo no existe '});
            }
            else{
                //LLAMAMOS AL MÉTODO GET PARA OBTENER LA INFORMACIÓN DEL LIBRO
                console.log(receiptBook.quota);
               var quotaId=receiptBook.quota;
               Quota.findById(quotaId).exec(function( err,quota){
                    if(err){
                        res.status(500).send({message: 'Error en la petición'});
                    }
                    else{
                        if(!quota){            
                            res.status(404).send({message: 'El libro no existe'});
                        }
                        else{
                            console.log(quota);
                            var update =quota;
                            update.total=update.total+receiptBook.cantidad;
                            var QuotaId=quota._id;
                           
                            Quota.findByIdAndUpdate(QuotaId,update, function(err, quotaUpdated){
                                if(err){
                                    res.status(500).send({message: 'Error al guardar el libro'});
                                }
                                else{
                                    if(!quotaUpdated){
                                        res.status(404).send({message: 'El libro no ha sido actualizado'});
                                    }
                                    else{
                                        Receipt.findByIdAndRemove(receiptId,function(err, receiptRemoved){
                                            if(err){
                                                res.status(500).send({message:'Error al eliminar la reserva'});
                                            }
                                            else{
                                                if(!receiptRemoved){
                                                    res.status(404).send({message:'La reserva ha sido eliminada'});
                                                }
                                                else{
                                                    res.status(200).send({receipt: receiptRemoved});
                                                }
                                            }
                                        });
                                    }
                                } 
                            });
                               
                            }
                          
                    }
                });
            }
        }
    });
                


  
}



/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getFactureSeller(req, res){
    var receiptId= req.params.id;
    var update = req.body;

    Receipt.findByIdAndUpdate(receiptId,update, function(err, receiptUpdated){
   
    if(err){
        res.status(500).send({message: 'Error en la actualización de la reserva'});
    }
    else{
        if(!receiptUpdated){
            res.status(404).send({message: 'La reserva no ha sido actualizada'});
        }
        else{
            res.status(200).send({receipt: receiptUpdated});
        }
    } });
}



function getReceiptByBook_Report_Category(req, res){

    var categoryId= req.params.category;


    if(bookId){
       
        var find= Receipt.find( {$and:[{category: categoryId}, {estado:'P'}  ]}).populate({path: 'user'}).populate({path:'quota'}).populate({path:'order'}).populate({path:'office'}).populate({path:'book'}).populate({path:'category'});
    }
    find.exec(function (err, receipt){ 
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!receipt){
                      res.status(404).send({message:'No hay libros'});
                  }else{
                    find.populate({path: 'book'}).exec(function (err, receiptBook){
                        if(err){
                            res.status(500).send({message: 'Error en la petición'});
                        }else{
                                if(!receiptBook){
                                    res.status(404).send({message:'No hay libros'});
                                }else{          

                                    //return res.status(200).send({receipt: receipt, receipt:receiptBook});
                                    find.populate({path: 'user'}).exec(function (err, receiptuser){
                                        if(err){
                                            res.status(500).send({message: 'Error en la petición'});
                                        }else{
                                                if(!receiptuser){
                                                    res.status(404).send({message:'No hay usurios'});
                                                }else{          
                                       //             return res.status(200).send({receipt: receipt, receipt:receiptBook, receipt:receiptuser});
                                                        find.populate({path: 'office'}).exec(function (err, receiptoffice){
                                                            if(err){
                                                                res.status(500).send({message: 'Error en la petición'});
                                                            }else{
                                                                    if(!receiptoffice){
                                                                        res.status(404).send({message:'No hay usurios'});
                                                                    }else{          
                                                                        return res.status(200).send({receipt: receipt, receipt:receiptBook, receipt:receiptuser, receipt:receiptoffice});
                                                                    }
                                                            }
                                                        });
                                                }
                                        }
                                    });
                                }
                        }
                    });
                  }
          }
      });



}

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getFactureOffice(req, res){
    var receiptId= req.params.id;
    var update = req.body;

    Receipt.findByIdAndUpdate(receiptId,update, function(err, receiptUpdated){
   
    if(err){
        res.status(500).send({message: 'Error en la actualización de la reserva'});
    }
    else{
        if(!receiptUpdated){
            res.status(404).send({message: 'La reserva no ha sido actualizada'});
        }
        else{
            res.status(200).send({receipt: receiptUpdated});
        }
    } });
}




module.exports={
    getReceiptById,
    getReceiptByUser,
    isReceiptEstadoR,
    getReceiptBySellerReport,
    saveReceipt,
    getReceiptByOffice,
    deleteReceipt,
    updateReceipt,
    getReceiptByOrder,
    deleteReceiptWithId,
    getReceiptByOfficeReport,
    getReceiptByOffice_Transference_Report,
    getReceiptByBook_Report,
    getReceiptByBook_Report_Category

}