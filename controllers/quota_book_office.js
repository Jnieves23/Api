'use strict'
var Quota= require('../models/quota_book_office');
var  Book= require('../models/book');


/***************
 * METODO     :getQuota
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getQuota(req, res){
    //Asignamos en una variable el parámetro id de la Quota enviada por el administrador
    var quotaId= req.params.id;
    //Realizamos la consulta según el id de la Quota
    Quota.findById(quotaId,(err,result)=>{
        if(err){//Si la petición obtuvo un error el sistema enviará una respuesta 500 con el siguiente mensaje
            res.status(500).send({message: 'Error en la petición'});
        }
        else{//Si la petición fue exitosa 
            if(!result){//Si la petición fue exitosa pero no contiene datos  el sistema enviará una respuesta 404 con el siguiente mensaje
                res.status(404).send({message: 'La cantidad no existe'});
            }
            else{//Si la petición fue exitosa pero no contiene datos  el sistema enviará una respuesta 200 con el siguiente mensaje    
                res.status(200).send({ quota: result});
            }
        }
    });
  
}

/***************
 * METODO     :saveQuota
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveQuota(req,res){
    var quota = new Quota();

    var params = req.body;
    console.log("params.book "+params.book);
    if(params.book){
        quota.book= params.book;
    }else{
        quota.book= "NULL";
    }
    
    quota.total= params.total;
    quota.quota_stock= params.quota_stock;
    quota.office=  params.office;
    quota.estado=  'S';

    quota.save((err,result)=>{
        if(err){
            res.status(500).send({message:'Error al guardar el autor'});
        }
        else{
            if(!result){
                res.status(404).send({message: 'La cuota no ha sido guardado'});
            }
            else{
                res.status(200).send({quota: result});
            }
        }
    });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getQuotas(req, res){
    var bookId=req.params.id;
    console.log(" bookId "+bookId);

    //{dscto:{$gt:0} } mayor a 0
    // var find= Receipt.find( {$and:[{user: sellerId}, {estado:'P'}  ]}).sort('estado').populate({path: 'user'}).populate({path:'quota'}).populate({path:'order'});
      Quota.find({$and:[{book:bookId},{quota_stock:{$gt:0} },{total:{$gt:0} } ,{estado:'S' }]}).populate({path: 'book'}).populate({path: 'office'}).exec( function (err, result){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
              if(!result){
                res.status(404).send({message: 'La couta no ha sido actualizado'});
              }else{
                        return res.status(200).send({ quotas:  result});
                    }
                          
          }
            });
  }





/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getQuotas_Add_Book(req, res){
    var bookId=req.params.id;
    console.log(" bookId "+bookId);

    //{dscto:{$gt:0} } mayor a 0
    // var find= Receipt.find( {$and:[{user: sellerId}, {estado:'P'}  ]}).sort('estado').populate({path: 'user'}).populate({path:'quota'}).populate({path:'order'});
      Quota.find({book:bookId}).populate({path: 'book'}).populate({path: 'office'}).exec( function (err, result){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
              if(!result){
                res.status(404).send({message: 'La couta no ha sido actualizado'});
              }else{
                  console.log("result",result);
                        return res.status(200).send({ quotas:  result});
                    }
                          
          }
            });
  }

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getQuotas_By_Office(req, res){
    var officeId=req.params.office;
    console.log(" officeId "+officeId);

    //{dscto:{$gt:0} } mayor a 0
    // var find= Receipt.find( {$and:[{user: sellerId}, {estado:'P'}  ]}).sort('estado').populate({path: 'user'}).populate({path:'quota'}).populate({path:'order'});
      Quota.find({office:officeId}).populate({path: 'book'}).populate({path: 'office'}).exec( function (err, result){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
              if(!result){
                res.status(404).send({message: 'La couta no ha sido actualizado'});
              }else{
                  console.log("result",result);
                        return res.status(200).send({ quotas:  result});
                    }
                          
          }
            });
  }





  /***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getQuota_office(req, res){
    var quotaId=req.params.id;
    //{$and:[{category:idCategory},{estado:'S' } ]}
      //Quota.find({$and:[{id:quotaId},{estado:'S' } ]}).populate({path: 'book'}).exec( function (err, result){
        Quota.findById(quotaId).populate({path: 'book'}).exec( function (err, result){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
              if(!result){
                res.status(404).send({message: 'La couta no ha sido actualizado'});
              }else{
                Quota.findById(quotaId).populate({path: 'office'}).exec( function (err, resultOffice){
                    if(err){
                        res.status(500).send({message: 'Error en la petición'});
                    }else{
                        if(!resultOffice){
                          res.status(404).send({message: 'La couta no ha sido actualizado'});
                        }else{
                           
                          return res.status(200).send({ quota:  result,quota:  resultOffice});
          
          
                          
                        }
                          
                          }
                      });


              }
                
                }
            });
  }

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateQuota(req, res){
    var quotaId= req.params.id;
    var update = req.body;
    
    //Realizamos la actualización de la cantidad  según el id de la Quota
    Quota.findByIdAndUpdate(quotaId,update, function(err, quotaUpdated){
   
    if(err){//Si la petición obtuvo un error el sistema enviará una respuesta 500 con el siguiente mensaje
        res.status(500).send({message: 'Error al actualizar la cuota'});
    }
    else{//Si la petición fue exitosa 
        if(!quotaUpdated){//Si la petición fue exitosa pero no contiene datos  el sistema enviará una respuesta 404 con el siguiente mensaje
            res.status(404).send({message: 'La cuota no ha sido actualizado'});
        }
        else{//Si la petición fue exitosa pero no contiene datos  el sistema enviará una respuesta 200 con el siguiente mensaje    
            res.status(200).send({quota: quotaUpdated});
        }
    } });

}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function deleteQuota( req, res){
    var quotaId = req.params.id;

    Quota.findByIdAndRemove(quotaId,function(err, quotaRemoved){
        if(err){
            res.status(500).send({message: ' Error al guardar la cuota'});
        }else{
                if(!quotaRemoved){
                    res.status(404).send({message: 'La cuota no ha sido eliminado'});
                }else{
                    //res.status(200).send({quotaRemoved});
                    Book.find({quota: quotaRemoved._id}).remove(function(err, bookRemoved){
                        if(err){
                            res.status(500).send({message:'Error al eliminar la cuota'});
                        }
                        else{
                            if(!bookRemoved){
                                res.status(404).send({message:'La cuota no ha sido eliminado'});
                            }
                            else{
                                res.status(200).send({quota: quotaRemoved});
                            }
                        }
                    });
                }
        }
    });
}



/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function clearQuota( req, res){
   

    
        Quota.remove({book:"NULL"}).exec(function(err, quotaRemoved){
        if(err){
            res.status(500).send({message: ' Error al eliminar la cuota'});
        }else{
                if(!quotaRemoved){
                    res.status(404).send({message: 'La cuota no ha sido eliminado'});
                }else{ 
                res.status(200).send({quota: quotaRemoved});
                }
        }
    });
}


module.exports={
    getQuota,
    getQuotas,
    getQuotas_Add_Book,
    getQuotas_By_Office,
    saveQuota,
    updateQuota,
    deleteQuota,
    clearQuota,
    getQuota_office
    
   
    

}