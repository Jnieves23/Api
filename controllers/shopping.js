'use strict'
var  Receipt= require('../models/receipt');
var  User= require('../models/user');
var  Book= require('../models/book');
var  Office= require('../models/office');
var  mongoosePaginate= require('mongoose-pagination');


var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'sajdjhabsda6s5d165as4d56a4s65d1as5d1as65d651asd';

/**
 * *********************************************************************
 * METODO: encrypt
 * DESCRIPPCION: ENCRIPTA LA CONTRASEÑA
 * AUTOR: Jnieves
 * FECHA CREACION: 06/01/2019
 * ************************************************************************
 */
function encrypt(text){
    var cipher = crypto.createCipher(algorithm,password)
    var crypted = cipher.update(text,'utf8','hex')
    crypted += cipher.final('hex');
    return crypted;
  }
   /**
   * *********************************************************************
   * METODO: decrypt
   * DESCRIPPCION: DESENCRIPTA LA CONTRASEÑA
   * AUTOR: Jnieves
   * FECHA CREACION: 06/01/2019
   * ************************************************************************
   */
  function decrypt(text){
    var decipher = crypto.createDecipher(algorithm,password)
    var dec = decipher.update(text,'hex','utf8')
    dec += decipher.final('utf8');
    return dec;
  }







/***************
 * METODO     : getReceipt
 * DESCRIPCION: Devuelve todas las reservas  de un usuario.
 * AUTOR      :  Jnieves
 * FECHA      :  02/12/2018
 * *************************** */
function getShoppingByUser(req, res){
    console.log("req.params "+req.params.id);

    var userId= req.params.id;

    if(userId){
        console.log("getShoppingByUser  ", "userId ",userId);
        var find= Receipt.find({$and:[{user: userId},{estado: 'R'}]}).sort('estado').populate({path: 'user'}).populate({path:'book'}).populate({path:'office'});
    }
    
    find.exec(function (err, receipt){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!receipt){
                      res.status(404).send({message:'No hay reservas'});
                  }else{    
                    if(receipt[0]){
                      receipt[0].user.password=(decrypt(receipt[0].user.password));
                    } 
                   //   console.log("receipt[0].user.password",receipt[0],receipt);
                    
                 
                    return res.status(200).send({receipt: receipt});
                 }                                             
                   
                } });
}





module.exports={
    getShoppingByUser,

}