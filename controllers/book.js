'use strict'
var fs = require('fs');
var path = require('path');

var  User= require('../models/user');
var Author= require('../models/author');
var Category= require('../models/category');
var Office= require('../models/office');
var  Book= require('../models/book');
var Quota= require('../models/quota_book_office');
var mongoosePaginate= require('mongoose-pagination');

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getBook(req, res){
    var bookId= req.params.id;
   
                Book.findById(bookId).populate({path: 'quantity'}).populate({path: 'author'}).populate({path: 'category'}).exec(function( err,quantity){
                    if(err){
                        res.status(500).send({message: 'Error en la petición'});
                    }
                    else{
                        if(!quantity){
                            res.status(404).send({message: 'El libro no existe'});
                        }
                        else{
                            
                            res.status(200).send({book:quantity});
                        }
                    }

                 } );
         
  }
 
/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveBook (req, res){
    var book = new Book();

    var params= req.body;
    book.title = params.title;
    book.description= params.description;
    book.year = params.year;
    book.image = 'null';
    book.category=params.category;
    book.estado='S';
   // book.office=params.office;
    
    book.author =params.author;

//---------------------------------------------------------//      
   book.price = params.price;
//   book.quantity=params.quantity;
//    book.office='5c0d58823c9e28054ccaac67';
//---------------------------------------------------------//  


    book.quantity = params.quantity;
    book.dscto    =params.dscto;


    book.save( (err, BookStored)=>{
        if(err){   
            console.log("err",err);
            res.status(500).send({message: 'Error en el servidor'});        
        }
        else{
                  if(!BookStored){
                    res.status(404).send({message: 'No se ha guardado el libro'});        
                }   
                else{
                    res.status(200).send({book: BookStored});        
                }         
        }
    });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getBooks(req, res){
    console.log(req.params);
    var authorId= req.params.author;
    if(! authorId|| authorId=='null'){
        console.log("entro aqui");
        var find= Book.find({estado: 'S'}).sort('title').populate({path: 'quantity'}).populate({path: 'author'}).populate({path: 'category'});
    }
    else{
         // Order.find( {$and:[{office: officeId}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}  ]}).populate({path: 'user'}).exec(function (err,orderEstado)
        var find= Book.find({$and:[{author: authorId},{estado: 'S'} ]}).sort('year');
    }

     
    find.populate({path: 'author'}).exec(function (err, books,total){
          if(err){
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!books){
                      res.status(404).send({message:'No hay libros'});
                  }else{
                      
                      return res.status(200).send({book: books});
                  }
          }
      });
  
  }
  
/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getBooksP(req, res){
  
      var page = req.params.page || 1;
      var itemsPerPage = Number(req.params.itemsperpage) || 12;
      var sort = req.params.sort || 'name';
      
  
  
      Book.find({estado: 'S'}).populate({path: 'quantity'}).populate({path: 'author'}).populate({path: 'category'}).paginate( page,itemsPerPage,  (err, books,total)=>{
          if(err){
              console.log(err);
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!books){
                      res.status(404).send({message:'No hay libros'});
                  }else{
                      return res.status(200).send({total_items: total, books:  books});
                  }
          }
      });
  
  }


  

  function getBookDeshabilitados(req, res){
  
    var page = req.params.page || 1;
    var itemsPerPage = Number(req.params.itemsperpage) || 12;
    var sort = req.params.sort || 'name';
    
    Book.find({estado: 'N'}).populate({path: 'quantity'}).populate({path: 'author'}).populate({path: 'category'}).paginate( page,itemsPerPage,  (err, books,total)=>{
        if(err){
            console.log(err);
            res.status(500).send({message: 'Error en la petición'});
        }else{
                if(!books){
                    res.status(404).send({message:'No hay libros'});
                }else{
                    return res.status(200).send({total_items: total, books:  books});
                }
        }
    });

}




/***************
 * METODO     : getBooksCategory
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getBooksCategory(req, res){
      var idCategory=req.params.category;
    
      Book.find({$and:[{category:idCategory},{estado:'S' } ]}).populate({path: 'quantity'}).populate({path: 'author'}).populate({path: 'category'}).exec(function(err, books){
          if(err){
              console.log(err);
              res.status(500).send({message: 'Error en la petición'});
          }else{
                  if(!books){
                      res.status(404).send({message:'No hay libros'});
                  }else{
                      return res.status(200).send({ books:  books});
                  }
          }
      });
  
  }

  /***************
 * METODO     : getBooksOferta
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getBooksOferta(req, res){

    var find;
    if(req.params.page=='null'){
        console.log('sin datos');
        find=Book.find({$and:[{dscto:{$gt:0} },{estado:'S' } ]}).populate({path: 'quantity'}).populate({path: 'author'});
    }else{
        console.log('con datos');
        var page = req.params.page || 1;
        var itemsPerPage = Number(req.params.itemsperpage) || 12;
        var sort = req.params.sort || 'name';

        // Order.find( {$and:[{office: officeId}, {$or: [{estado: 'PP'}, {estado: 'PE'}]}  ]}).populate({path: 'user'}).exec(function (err,orderEstado)
        find= Book.find({$and:[{dscto:{$gt:0} },{estado:'S' } ]}).populate({path: 'quantity'}).populate({path: 'author'}).paginate( page,itemsPerPage);
    }
   
    
  
   find.exec( function( err, books,total){
        if(err){
            console.log(err);
            res.status(500).send({message: 'Error en la petición'});
        }else{
                if(!books){
                    res.status(404).send({message:'No hay libros'});
                }else{
                    return res.status(200).send({total_items: total, books:  books});
                }
        }
    });

}
 /***************
 * METODO     : getBooksOferta_page
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getBooksOferta_parametrizable(req, res){

    console.log(req.body.page);
    var find;
    if(req.body.page=='sin'){
        console.log('sin datos');
        find=Book.find({dscto:{$gt:0} }).sort('name').populate({path: 'quantity'}).populate({path: 'category'}).populate({path: 'author'});
    }else{
        console.log('con datos');
        var page = req.body.page || 1;
        var itemsPerPage = Number(req.body.itemsperpage) || 3;
        var sort = req.body.sort || 'name';

        find= Book.find({dscto:{$gt:0} }).sort(sort).populate({path: 'quantity'}).populate({path: 'category'}).populate({path: 'author'}).paginate( page,itemsPerPage);
    }


    
   // Book.find({dscto:{$gt:0} }).sort(sort).populate({path: 'quantity'}).populate({path: 'category'}).populate({path: 'author'}).paginate( page,itemsPerPage,  (err, books,total)=>{
    find.exec(function(err, books,total){
        if(err){
            console.log(err);
            res.status(500).send({message: 'Error en la petición'});
        }else{
                if(!books){
                    res.status(404).send({message:'No hay libros'});
                }else{
                    return res.status(200).send({total_items: total, books:  books});
                }
        }
    });

}



/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateBook(req, res){
    var bookId= req.params.id;
    var update = req.body;
   
    Book.findByIdAndUpdate(bookId,update, function(err, bookUpdated){
   
    if(err){
       
        res.status(500).send({message: 'Error al guardar el libro'});
    }
  
    else{
        if(!bookUpdated){
            res.status(404).send({message: 'El libro no ha sido actualizado'});
        }
        else{
            res.status(200).send({book: bookUpdated});
        }
    } });

}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function deleteBook( req, res){
    var bookId = req.params.id;




    Book.findByIdAndRemove(bookId,function(err, bookRemoved){
        if(err){
            res.status(500).send({message:'Error al eliminar el libro'});
        }
        else{
            if(!bookRemoved){
                res.status(404).send({message:'El libro ha sido eliminado'});
            }
            else{
                //res.status(200).send({book: bookRemoved});

                Quota.find({book: bookRemoved._id}).remove(function(err, Removed){
                    if(err){
                        res.status(500).send({message:'Error al eliminar la quota'});
                    }
                    else{
                        if(!Removed){
                            res.status(404).send({message:'La quota no ha sido eliminado'});
                        }
                        else{
                            res.status(200).send({book: Removed, book:bookRemoved });
                        }
                    }
                });



            }
        }
    });
}


/***************
 * METODO     : uploadImage
 * DESCRIPCION: 
 * AUTOR      : Jnieves
 * FECHA      : 08/11/2018
 * *************************** */
function uploadImage(req,res){
    var bookId= req.params.id;
    var filename='No subido...';

    if(req.files){
        var file_path= req.files.image.path;
        console.log(file_path);
        var file_split=file_path.split('\\');
        var file_name=file_split[2];
        var exr_explit= file_name.split('\.');
        var file_ext=exr_explit[1];//extención

        if(file_ext =='png' || file_ext=='jpg'|| file_ext =='gif'){
            Book.findByIdAndUpdate(bookId,{image:file_name }, (err,bookUpdate)=>{
                if(err){
                    res.status(500).send({message: 'No se ha podido cargar el fichero'});
                }
                else{
                    if(!bookUpdate){
                    res.status(404).send({message: 'No se ha encontrado el libro'});
                    }
                    else{
                        res.status(200).send({book: bookUpdate});
                    }
                }
            });
        }
        else{
            res.status(200).send({message: 'Extención de la imagen no valida'});
        }
    }
    else{
        res.status(200).send({message: 'No has subido ninguna imagen..'});
    }
}


/***************
 * METODO     : getImageFile
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function getImageFile(req, res){
    var image_file= req.params.imageFile;
    var path_file = './uploads/books/'+image_file;
    fs.exists(path_file,function(exists){
        if(exists){
            res.sendFile (path.resolve(path_file));
        }
        else{
            res.status(200).send({message: 'No existe la imagen'});        
        }
    });
}

function getSearchBooks(){
    var search= req.params.id;
   
                Book.findById(bookId).populate({path: 'quantity'}).populate({path: 'author'}).populate({path: 'category'}).exec(function( err,quantity){
                    if(err){
                        res.status(500).send({message: 'Error en la petición'});
                    }
                    else{
                        if(!quantity){
                            res.status(404).send({message: 'El libro no existe'});
                        }
                        else{
                            
                            res.status(200).send({book:quantity});
                        }
                    }

                 } );
         
}


module.exports={
    getBook,
    saveBook ,
    getBooks,
    updateBook   ,
    deleteBook,
    getImageFile,
    uploadImage,
    getBooksP,
    getBookDeshabilitados,
    getBooksCategory,
    getBooksOferta,
    getBooksOferta_parametrizable,
    getSearchBooks


}