'use strict'
var User= require('../models/user');
var bcrypt= require('bcrypt-nodejs');
var fs = require('fs');
var path = require('path');

var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'sajdjhabsda6s5d165as4d56a4s65d1as5d1as65d651asd';

 /**
 * *********************************************************************
 * METODO: decrypt
 * DESCRIPPCION: DESENCRIPTA LA CONTRASEÑA
 * AUTOR: Jnieves
 * FECHA CREACION: 06/01/2019
 * ************************************************************************
 */
function decrypt(text){
    var decipher = crypto.createDecipher(algorithm,password)
    var dec = decipher.update(text,'hex','utf8')
    dec += decipher.final('utf8');
    return dec;
  }

  /**
 * *********************************************************************
 * METODO: encrypt
 * DESCRIPPCION: ENCRIPTA LA CONTRASEÑA
 * AUTOR: Jnieves
 * FECHA CREACION: 06/01/2019
 * ************************************************************************
 */
function encrypt(text){
    var cipher = crypto.createCipher(algorithm,password)
    var crypted = cipher.update(text,'utf8','hex')
    crypted += cipher.final('hex');
    return crypted;
  }

  /**
 * *********************************************************************
 * METODO: getSellers
 * DESCRIPPCION: ENCRIPTA LA CONTRASEÑA
 * AUTOR: Jnieves
 * FECHA CREACION: 06/01/2019
 * ************************************************************************
 */
function getSellers(req, res){
    var page = req.params.page;


    if( req.params.page=='null' ){
        console.log("nULL");
        var find= User.find({role:'ROLE_SELLER'}).sort('name');

        find.exec(function (err, sellers){
            if(err){
                res.status(500).send({message: 'Error en la petición'});
            }else{
                    if(!sellers){
                        res.status(404).send({message:'No hay vendedores'});
                    }else{
  
                      
                        return res.status(200).send({User: sellers});
                    }
            }
        });
    }
    else{
        console.log("PAGE");
        var page = req.params.page || 1;
        var itemsPerPage = Number(req.params.itemsperpage) || 4;
        var sort = req.params.sort || 'name';
        User.find({role:'ROLE_SELLER'}).sort('name').paginate( page,itemsPerPage, function (err, sellers,total){
            if(err){
                res.status(500).send({message: 'Error en la petición'});
            }else{
                    if(!User){
                        res.status(404).send({message:'No hay vendedores'});
                    }else{
                        return res.status(200).send({total_items: total,User:sellers});
                    }
            
        }});
    }



   
}

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getSeller(req, res){
    var sellerId= req.params.id;
    User.findById(sellerId,(err,seller)=>{
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }
        else{
            if(!seller){
                res.status(404).send({message: 'El vendedor no existe'});
            }
            else{
              //  seller.password=decrypt(seller.password);
                res.status(200).send({User:seller});
            }
        }
    });
  
}




/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveSeller(req,res){
    var schedule = new Schedule();

    var params = req.body;
    schedule.name= params.name;
    schedule.monday= params.monday;
    schedule.tuesday= params.tuesday;
    schedule.wednesday= params.wednesday;
    schedule.thursday= params.thursday;
    schedule.friday= params.friday;
    schedule.saturday= params.saturday;
    schedule.sunday= params.sunday;

    schedule.save((err,scheduleStored)=>{
        if(err){
            req.status(500).send({message:'Error al guardar el horario'});
        }
        else{
            if(!scheduleStored){
                req.status(404).send({message: 'El horario no ha sido guardado'});
            }
            else{
                res.status(200).send({schedule: scheduleStored});
            }
        }
    });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateSeller(req, res){
    var sellerId= req.params.id;
    var update = req.body;

    User.findByIdAndUpdate(sellerId,update,(err, sellerUpdated)=>{   
        if(err){
            res.status(500).send({message: 'Error al guardar el vendedor'});
        }
        else{
            if(!sellerUpdated){
                res.status(404).send({message: 'El vendedor no ha sido actualizado'});
            }
            else{
                res.status(200).send({User: sellerUpdated});
            }
        } });

}





/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateSellerPassword(req, res){
    var sellerId= req.params.id;
    var update = req.body;
 
  //  bcrypt.hash(update.password,null,null,function(err,hash){
 //       update.password= hash;
        update.password=encrypt(update.password);
        if(update.name != null && update.surname!=null && update.email !=null){
            User.findByIdAndUpdate(sellerId,update,(err, sellerUpdated)=>{   
                if(err){
                    res.status(500).send({message: 'Error al guardar el vendedor'});
                }
                else{
                    if(!sellerUpdated){
                        res.status(404).send({message: 'El vendedor no ha sido actualizado'});
                    }
                    else{
                        res.status(200).send({User: sellerUpdated});
                    }
                } });
        }
        else{
            res.status(200).send({message:'Rellena todos los campos'});
        }
  //  });   

}





/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function deleteSeller( req, res){
    var deleteId = req.params.id;

    User.findByIdAndRemove(deleteId,function(err, sellerRemoved){
        if(err){
            res.status(500).send({message: ' Error al eliminar el vendedor'});
        }else{
                if(!sellerRemoved){
                    res.status(404).send({message: 'El vendedor no ha sido eliminado'});
                }else{
                
                                res.status(200).send({User: sellerRemoved});
                       
                }
        }
    });
}

/***************
 * METODO     : getImageFile
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function getImageFile(req, res){
    var image_file= req.params.imageFile;
    var path_file = './uploads/users/'+image_file;
    fs.exists(path_file,function(exists){
        if(exists){
            res.sendFile (path.resolve(path_file));
        }
        else{
            res.status(200).send({message: 'No existe la imagen'});        
        }
    });
}


module.exports={
getSeller,
getSellers,
saveSeller,
updateSeller,
updateSellerPassword,
deleteSeller,
getImageFile
}