const paypal = require('paypal-rest-sdk');
var request = require('request');
var querystring = require('querystring');

paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AV4XaqJULq_EhA1onoJlSQUgXhJzm5gW0t6CThsV_TBjJK8Fo_GBmVckfOeqpzwNCN64bd3Bzs5vK-FS',
  'client_secret': 'EDnkfmfNoFSpxo6vc5SNjqH7tMFzmk6HLtzkVR_u4bsKJlEiydjZ8-DU2GH03tstSEG3jGXXSY4mEXQ4'
});




/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function SendPaypal(req, res){
    
    var params=req.body
    console.log("params "+params);
    console.log("params "+params._id);
    var id=params.id;


    var url_return="http://localhost:4200/mis-reservas/"+"5c211bf575f14746b0eb819f";

    console.log("SendPaypal()");
    const create_payment_json = {
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": "http://localhost:3000/api/success",
            "cancel_url": "http://localhost:3000/api/success"
        },
        "transactions": [{
            "item_list": {
                "items": [{
                    "name": "Red Sox Hat",
                    "sku": "001",
                    "price": "25.00",
                    "currency": "USD",
                    "quantity": 1
                }]
            },
            "amount": {
                "currency": "USD",
                "total": "25.00"
            },
            "description": "Hat for the best team ever"
        }]
    };
    
    paypal.payment.create(create_payment_json, function (error, payment) {
      if (error) {
          throw error;
      } else {
          for(let i = 0;i < payment.links.length;i++){
            if(payment.links[i].rel === 'approval_url'){
            //    res.send({ paypal:payment.links[i].href});
              res.redirect(payment.links[i].href);            
           //console.log(payment.links[i].href);
        }
            //res.send('Success'); 
          }
      }
    });

}



/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getSuccess(){
    const payerId = req.query.PayerID;
    const paymentId = req.query.paymentId;
  
    const execute_payment_json = {
      "payer_id": payerId,
      "transactions": [{
          "amount": {
              "currency": "USD",
              "total": "25.00"
          }
      }]
    };
  
    paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
      if (error) {
          console.log(error.response);
          throw error;
      } else {
          console.log(JSON.stringify(payment));
          res.send('Success');
      }
  });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getcancel(req, res){
    res.send('Cancelled');
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function postData(req, res){
    req.body = req.body || {};    
	res.status(200).send('OK');
    res.end();
    req.body = JSON.parse(JSON.stringify(req.body));
    var postreq = 'cmd=_notify-validate';
	for (var key in req.body) {
		if (req.body.hasOwnProperty(key)) {
			var value = querystring.escape(req.body[key]);
			postreq = postreq + "&" + key + "=" + value;
		}
	}

	// Step 2: POST IPN data back to PayPal to validate
//	console.log('Posting back to paypal'.bold);
	console.log(postreq);
	console.log('\n\n');
	var options = {
		url: 'https://www.sandbox.paypal.com/cgi-bin/webscr',
		method: 'POST',
		headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: postreq,
		strictSSL: true,
		rejectUnauthorized: false,
		requestCert: true,
		agent: false
	};

	request(options, function callback(error, response, body) {
		if (!error && response.statusCode === 200) {

			// inspect IPN validation result and act accordingly
			if (body.substring(0, 8) === 'VERIFIED') {
				// The IPN is verified, process it
				console.log('Verified IPN!');
				console.log('\n\n');

				// assign posted variables to local variables
				var item_name = req.body['item_name'];
				var item_number = req.body['item_number'];
				var payment_status = req.body['payment_status'];
				var payment_amount = req.body['mc_gross'];
				var payment_currency = req.body['mc_currency'];
				var txn_id = req.body['txn_id'];
				var receiver_email = req.body['receiver_email'];
				var payer_email = req.body['payer_email'];


/*
	El estado del pago:
    Canceled_Reversal : Se ha cancelado una anulación. Por ejemplo, 
                        ganó una disputa con el cliente y se le han devuelto 
                        los fondos para la transacción que se revirtió.

    Completed : el pago se completó y los fondos se agregaron con éxito al saldo de su cuenta.

    Created : un pago ELV alemán se realiza mediante Pago exprés.

    Denied : El pago fue denegado. Esto sucede solo si el pago estaba pendiente anteriormente 
            debido a una de las razones enumeradas para la variable pending_reason o la variable 
            Fraud_Management_Filters_ x .

    Expired : esta autorización ha caducado y no se puede capturar.
    Failed : el pago ha fallado. Esto sucede solo si el pago se realizó desde la cuenta bancaria de su cliente.
    Pending : El pago está pendiente. Vea pending_reason para más información.

    Refunded : Usted reembolsó el pago.

    Reversed : un pago se revirtió debido a un contracargo u otro tipo de revocación. Los fondos 
                se eliminaron del saldo de su cuenta y se devolvieron al comprador. 
                El motivo de la ReasonCode se especifica en el elemento ReasonCode .
    Processed : Se ha aceptado un pago.

    Voided : Esta autorización ha sido anulada.
*/
				console.log("payment_status:", payment_status)
				console.log('\n\n');
				for (var key in req.body) {
					if (req.body.hasOwnProperty(key)) {
						var value = req.body[key];
						console.log(key + "=" + value);
					}
                }
                
                if(payment_status==='Completed'){
                    console.log("SE PAGÓ");
                }
			} else if (body.substring(0, 7) === 'INVALID') {
				// IPN invalid, log for manual investigation
				console.log('Invalid IPN!');
				console.log('\n\n');
			}
		}
	});






}



module.exports={
    getSuccess,
    SendPaypal,
    getcancel,
    postData
    }