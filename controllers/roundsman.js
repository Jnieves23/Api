'use strict'
var User= require('../models/user');
var bcrypt= require('bcrypt-nodejs');
var fs = require('fs');
var path = require('path');
const fileUpload = require('express-fileupload')

var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'sajdjhabsda6s5d165as4d56a4s65d1as5d1as65d651asd';

 /**
 * *********************************************************************
 * METODO: decrypt
 * DESCRIPPCION: DESENCRIPTA LA CONTRASEÑA
 * AUTOR: Jnieves
 * FECHA CREACION: 06/01/2019
 * ************************************************************************
 */
function decrypt(text){
    var decipher = crypto.createDecipher(algorithm,password)
    var dec = decipher.update(text,'hex','utf8')
    dec += decipher.final('utf8');
    return dec;
  }

  /**
 * *********************************************************************
 * METODO: encrypt
 * DESCRIPPCION: ENCRIPTA LA CONTRASEÑA
 * AUTOR: Jnieves
 * FECHA CREACION: 06/01/2019
 * ************************************************************************
 */
function encrypt(text){
    var cipher = crypto.createCipher(algorithm,password)
    var crypted = cipher.update(text,'utf8','hex')
    crypted += cipher.final('hex');
    return crypted;
  }

  /**
 * *********************************************************************
 * METODO: getRoundsmans
 * DESCRIPPCION: ENCRIPTA LA CONTRASEÑA
 * AUTOR: Jnieves
 * FECHA CREACION: 06/01/2019
 * ************************************************************************
 */
function getRoundsmans(req, res){
    var page = req.params.page;


    if( req.params.page=='null' ){
        var find= User.find({role:'ROLE_ROUNDSMAN'}).sort('name');

        find.exec(function (err, roundsmans){
            if(err){
                res.status(500).send({message: 'Error en la petición'});
            }else{
                    if(!roundsmans){
                        res.status(404).send({message:'No hay vendedores'});
                    }else{
  
                      
                        return res.status(200).send({User: roundsmans});
                    }
            }
        });
    }
    else{
        var page = req.params.page || 1;
        var itemsPerPage = Number(req.params.itemsperpage) || 4;
        var sort = req.params.sort || 'name';
        User.find({role:'ROLE_ROUNDSMAN'}).sort('name').paginate( page,itemsPerPage, function (err, roundsmans,total){
            if(err){
                res.status(500).send({message: 'Error en la petición'});
            }else{
                    if(!User){
                        res.status(404).send({message:'No hay vendedores'});
                    }else{
                        return res.status(200).send({total_items: total,User:roundsmans});
                    }
            
        }});
    }



   
}

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getRoundsman(req, res){
    var roundsmanId= req.params.id;
    User.findById(roundsmanId,(err,roundsman)=>{
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }
        else{
            if(!roundsman){
                res.status(404).send({message: 'El vendedor no existe'});
            }
            else{
               // roundsman.password=decrypt(roundsman.password);
                res.status(200).send({User:roundsman});
            }
        }
    });
  
}




/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveRoundsman(req,res){
    var schedule = new Schedule();

    var params = req.body;
    schedule.name= params.name;
    schedule.monday= params.monday;
    schedule.tuesday= params.tuesday;
    schedule.wednesday= params.wednesday;
    schedule.thursday= params.thursday;
    schedule.friday= params.friday;
    schedule.saturday= params.saturday;
    schedule.sunday= params.sunday;

    schedule.save((err,scheduleStored)=>{
        if(err){
            req.status(500).send({message:'Error al guardar el horario'});
        }
        else{
            if(!scheduleStored){
                req.status(404).send({message: 'El horario no ha sido guardado'});
            }
            else{
                res.status(200).send({schedule: scheduleStored});
            }
        }
    });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateRoundsman(req, res){
    var roundsmanId= req.params.id;
    var update = req.body;

    User.findByIdAndUpdate(roundsmanId,update,(err, roundsmanUpdated)=>{   
        if(err){
            res.status(500).send({message: 'Error al guardar el vendedor'});
        }
        else{
            if(!roundsmanUpdated){
                res.status(404).send({message: 'El vendedor no ha sido actualizado'});
            }
            else{
                res.status(200).send({User: roundsmanUpdated});
            }
        } });

}





/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateRoundsmanPassword(req, res){
    var roundsmanId= req.params.id;
    var update = req.body;
 
  //  bcrypt.hash(update.password,null,null,function(err,hash){
 //       update.password= hash;
        update.password=encrypt(update.password);
        if(update.name != null && update.surname!=null && update.email !=null){
            User.findByIdAndUpdate(roundsmanId,update,(err, roundsmanUpdated)=>{   
                if(err){
                    res.status(500).send({message: 'Error al guardar el vendedor'});
                }
                else{
                    if(!roundsmanUpdated){
                        res.status(404).send({message: 'El vendedor no ha sido actualizado'});
                    }
                    else{
                        res.status(200).send({User: roundsmanUpdated});
                    }
                } });
        }
        else{
            res.status(200).send({message:'Rellena todos los campos'});
        }
  //  });   

}





/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function deleteRoundsman( req, res){
    var deleteId = req.params.id;

    User.findByIdAndRemove(deleteId,function(err, roundsmanRemoved){
        if(err){
            res.status(500).send({message: ' Error al eliminar el vendedor'});
        }else{
                if(!roundsmanRemoved){
                    res.status(404).send({message: 'El vendedor no ha sido eliminado'});
                }else{
                
                                res.status(200).send({User: roundsmanRemoved});
                       
                }
        }
    });
}

/***************
 * METODO     : getImageFile
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function getImageFile(req, res){
    var image_file= req.params.imageFile;
    var path_file = './uploads/users/'+image_file;
    fs.exists(path_file,function(exists){
        if(exists){
            res.sendFile (path.resolve(path_file));
        }
        else{
            res.status(200).send({message: 'No existe la imagen'});        
        }
    });
}

// function  uploadFile(req, res){
//     //console.log(req.body.file);
// console.log("req "+req.files);

//     let EDFile = req.files.file
//     // let EDFile = req.files.file

//     EDFile.mv(`./files/${EDFile.name}`,err => {
//         if(err) return res.status(500).send({ message : err })

//         return res.status(200).send({ message : 'File upload' })
//     }) 
//    // return res.status(200).send({ message : 'File upload' })
// }

 



module.exports={
getRoundsman,
getRoundsmans,
saveRoundsman,
updateRoundsman,
updateRoundsmanPassword,
deleteRoundsman,
getImageFile,
}