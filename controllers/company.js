'use strict'
var fs = require('fs');
var path = require('path');

var Company= require('../models/company');
var  Office= require('../models/office');
var mongoosePaginate= require('mongoose-pagination');

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function getCompany(req, res){
    var companyId= req.params.id;
    Company.findById(companyId,(err,company)=>{
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }
        else{
            if(!company){
                res.status(404).send({message: 'La empresa no existe'});
            }
            else{
                res.status(200).send({ company:company});
            }
        }
    });
  
}

/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function saveCompany(req,res){
    var company = new Company();

    var params = req.body;
    company.name= params.name;
    company.description= params.description;
    company.image= 'null';

    company.save((err,companyStored)=>{
        if(err){
            req.status(500).send({message:'Error al guardar la empresa'});
        }
        else{
            if(!companyStored){
                req.status(404).send({message: 'La empresa no ha sido guardada'});
            }
            else{
                res.status(200).send({company: companyStored});
            }
        }
    });
}


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function updateCompany(req, res){
    var companyId= req.params.id;
    var update = req.body;
//findOneAndUpdate
    Company.findByIdAndUpdate(companyId,update, function(err, companyUpdated){
   
    if(err){
        res.status(500).send({message: 'Error al guardar la empresa'});
    }
    else{
        if(!companyUpdated){
            res.status(404).send({message: 'La empresa no ha sido actualizada'});
        }
        else{
            res.status(200).send({company: companyUpdated});
        }
    } });

}


/***************
 * METODO     : uploadImage
 * DESCRIPCION: 
 * AUTOR      : Jnieves
 * FECHA      : 08/11/2018
 * *************************** */
function uploadImage(req,res){
    var companyId= req.params.id;
    var filename='No subido...';

    if(req.files){
        var file_path= req.files.image.path;
        console.log(file_path);
        var file_split=file_path.split('\\');
        var file_name=file_split[2];
        var exr_explit= file_name.split('\.');
        var file_ext=exr_explit[1];//extención

        if(file_ext =='png' || file_ext=='jpg'|| file_ext =='gif'){
            Company.findByIdAndUpdate(companyId,{image:file_name }, (err,companyUpdate)=>{
                if(err){
                    res.status(500).send({message: 'No se ha podido cargar el fichero'});
                }
                else{
                    if(!companyUpdate){
                    res.status(404).send({message: 'No se ha encontrado la empresa'});
                    }
                    else{
                        res.status(200).send({company: companyUpdate});
                    }
                }
            });
        }
        else{
            res.status(200).send({message: 'Extención de la imagen no valida'});
        }
    }
    else{
        res.status(200).send({message: 'No has subido ninguna imagen..'});
    }
}


/***************
 * METODO     : getImageFile
 * DESCRIPCION: Actualiza un usuario
 * AUTOR      : Jnieves
 * FECHA      : 07/11/2018
 * *************************** */
function getImageFile(req, res){
    var image_file= req.params.imageFile;
    var path_file = './uploads/company/'+image_file;
    fs.exists(path_file,function(exists){
        if(exists){
            res.sendFile (path.resolve(path_file));
        }
        else{
            res.status(200).send({message: 'No existe la imagen'});        
        }
    });
}





module.exports={
    getCompany,
    saveCompany,
    updateCompany,
    uploadImage,
    getImageFile
    

}