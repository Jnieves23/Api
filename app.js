'use strict'

var express = require ('express');
var bodyParser = require ('body-parser');
const cors = require('cors');
var fileUpload = require('express-fileupload');
var app= express();


/* INI CARGAR RUTAS*/
var user_routers= require('./routers/user');
var author_routers= require('./routers/author');
var book_routers = require ('./routers/book');
var receipt_routers = require ('./routers/receipt');
var company_routers = require ('./routers/company');
var office_routers = require ('./routers/office');
var schedule_routers = require ('./routers/schedule');
var seller_routers = require ('./routers/seller');
var roundsman_routers = require ('./routers/roundsman');
var config_Mensaje_routers = require ('./routers/configMensaje');
var shopping = require ('./routers/shopping');
var order = require ('./routers/order');
var paypal = require ('./routers/paypal');
var quota = require ('./routers/quota_book_office');
var category = require ('./routers/category');
/* FIN CARGAR RUTAS*/



app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());// convierte a formato json


// Configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

/* CONFIGURAR CABECERAS*/

/* RUTAS BASE*/
app.use('/api',user_routers);
app.use('/api',author_routers);
app.use('/api',book_routers);
app.use('/api',receipt_routers);
app.use('/api',company_routers);
app.use('/api',office_routers);
app.use('/api',schedule_routers);
app.use('/api',seller_routers);
app.use('/api',roundsman_routers);
app.use('/api',config_Mensaje_routers);
app.use('/api',shopping);
app.use('/api',order);
app.use('/api',quota);

app.use('/api', paypal);
app.use('/api',category);
/*FIN RUTAS BASE*/


app.use(cors());

app.use(fileUpload());
module.exports= app;