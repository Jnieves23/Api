'use strict'

var express= require ('express');
var OrderController= require('../controllers/order');

var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 

var multipart= require ('connect-multiparty'); //permite trabajar con los ficheros
var md_upload= multipart({uploadDir: './files'});//donde se van a subir los ficheros

api.post('/order',md_auth.ensureAuth, OrderController.saveOrder);//agregué el token encriptado
api.get('/orderByUser/:id',md_auth.ensureAuth, OrderController.getOrderByUser);//agregué el token encriptado
api.get('/orderById/:id',md_auth.ensureAuth, OrderController.getOrderById);//agregué el token encriptado
api.get('/orderByRundsman/:roundsman',md_auth.ensureAuth, OrderController.getOrderByRundsman);//agregué el token encriptado
api.get('/orderByRundsmanReport/:roundsman',md_auth.ensureAuth, OrderController.getOrderByRundsmanReport);//agregué el token encriptado


api.get('/orderByCedulaPay/:cedula',md_auth.ensureAuth, OrderController.getOrderByCedulaPay);//agregué el token encriptado
api.get('/orderByIdPay/:order',md_auth.ensureAuth, OrderController.getOrderByIdPay);//agregué el token encriptado


api.get('/orderByRundsman_pdfComprobante/:roundsman',md_auth.ensureAuth, OrderController.getOrderByRundsman_pdfComprobante);//agregué el token encriptado
api.post('/order_filter_my_router_list',md_auth.ensureAuth, OrderController.getOrder_filter_my_router_list);//agregué el token encriptado



api.delete('/order/:id',md_auth.ensureAuth, OrderController.deleteOrder);//agregué el token encriptado

api.put('/order/:id',md_auth.ensureAuth, OrderController.updateOrder);//agregué el token encriptado

api.get('/isOrderR/:id',md_auth.ensureAuth, OrderController.isOrdertEstadoR);
api.get('/orderByCedula/:cedula',OrderController.getOrderByCedula);
api.get('/ordertByPAndSeller/:office',md_auth.ensureAuth, OrderController.getOrdertByPAndSeller);//agregué el token encriptado

api.post('/upload/:id' , [ md_auth.ensureAuth,md_upload ],OrderController.uploadFile);/*:id es obligatorio*/
api.get('/get-file-pdf/:pdf_comprobanteFile', OrderController.getFile);/*:id es obligatorio*/

module.exports= api;