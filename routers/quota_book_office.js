'use strict'

var express= require ('express');
var QuotaBookOfficeController= require('../controllers/quota_book_office');

var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 




api.get('/quota/:id',md_auth.ensureAuth, QuotaBookOfficeController.getQuota);//agregué el token encriptado
api.post('/quota',md_auth.ensureAuth, QuotaBookOfficeController.saveQuota);//agregué el token encriptado

api.get('/quotas_add_book/:id',md_auth.ensureAuth, QuotaBookOfficeController.getQuotas_Add_Book);//agregué el token encriptado 
api.get('/quotas_by_office/:office',md_auth.ensureAuth, QuotaBookOfficeController.getQuotas_By_Office);//agregué el token encriptado 


api.get('/quotas/:id',md_auth.ensureAuth, QuotaBookOfficeController.getQuotas);//agregué el token encriptado
api.get('/quotas_office/:id',md_auth.ensureAuth, QuotaBookOfficeController.getQuota_office);//agregué el token encriptado

api.delete('/quota/:id',md_auth.ensureAuth, QuotaBookOfficeController.deleteQuota);//agregué el token encriptado
api.delete('/clear_quota',md_auth.ensureAuth, QuotaBookOfficeController.clearQuota);//agregué el token encriptado


api.put('/quota/:id',md_auth.ensureAuth, QuotaBookOfficeController.updateQuota);//agregué el token encriptado

module.exports= api;