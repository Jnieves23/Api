'use strict'

var express= require ('express');
var BookController= require('../controllers/book');

var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 
var multipart= require ('connect-multiparty'); //permite trabajar con los ficheros
var md_upload= multipart({uploadDir: './uploads/books'});//donde se van a subir los ficheros



api.get('/book/:id', BookController.getBook);//agregué el token encriptado
api.get('/SearchBooks/:search', BookController.getSearchBooks);//agregué el token encriptado



api.post('/book',md_auth.ensureAuth, BookController.saveBook);//agregué el token encriptado
api.get('/books/:author?', BookController.getBooks);//agregué el token encriptado
api.delete('/book/:id',md_auth.ensureAuth, BookController.deleteBook);//agregué el token encriptado
api.post('/upload-image-book/:id', [ md_auth.ensureAuth,md_upload ] , BookController.uploadImage);/*:id es obligatorio*/
api.get('/get-image-book/:imageFile', BookController.getImageFile);/*:id es obligatorio*/
api.put('/updateBook/:id',md_auth.ensureAuth, BookController.updateBook);/*:id es obligatorio*/
api.get('/booksP/:page?',BookController.getBooksP);//agregué el token encriptado
api.get('/bookDeshabilitados/:page?',BookController.getBookDeshabilitados);//agregué el token encriptado

api.get('/books_category/:category',BookController.getBooksCategory);//agregué el token encriptado
api.get('/booksOferta/:page?',BookController.getBooksOferta);//agregué el token encriptado
api.post('/booksOferta_parametrizable',BookController.getBooksOferta_parametrizable);//agregué el token encriptado


//api.get('/book/:id',md_auth.ensureAuth, BookController.getBook);//agregué el token encriptado
//api.get('/books/:author?',md_auth.ensureAuth, BookController.getBooks);//agregué el token encriptado
//api.get('/book/:id', BookController.getBook);//agregué el token encriptado
//api.get('/booksP/:page?',md_auth.ensureAuth,BookController.getBooksP);//agregué el token encriptado
module.exports= api;