'use strict'

var express= require ('express');
var OfficeController= require('../controllers/office');

var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 
var multipart= require ('connect-multiparty'); //permite trabajar con los ficheros
var md_upload= multipart({uploadDir: './uploads/offices'});//donde se van a subir los ficheros



api.get('/office/:id', OfficeController.getOffice);//agregué el token encriptado
api.post('/office',md_auth.ensureAuth, OfficeController.saveOffice);//agregué el token encriptado
api.get('/offices/:page?', OfficeController.getOffices);//agregué el token encriptado
api.delete('/office/:id',md_auth.ensureAuth, OfficeController.deleteOffice);//agregué el token encriptado
api.post('/upload-image-office/:id', [ md_auth.ensureAuth,md_upload ] , OfficeController.uploadImage);/*:id es obligatorio*/
api.get('/get-image-office/:imageFile', OfficeController.getImageFile);/*:id es obligatorio*/
api.put('/updateOffice/:id',md_auth.ensureAuth, OfficeController.updateOffice);/*:id es obligatorio*/
api.get('/officesP/:page?',OfficeController.getOfficesP);//agregué el token encriptado
module.exports= api;