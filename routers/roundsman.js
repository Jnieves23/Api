'use strict'

var express= require ('express');
var UseController= require('../controllers/user');
var RoundsmanController= require('../controllers/roundsman');


var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 



api.get('/roundsmans/:page?',md_auth.ensureAuth,RoundsmanController.getRoundsmans);//agregué el token encriptado
api.get('/roundsman/:id',md_auth.ensureAuth,RoundsmanController.getRoundsman);//agregué el token encriptado
api.put('/updateRoundsman/:id',md_auth.ensureAuth,RoundsmanController.updateRoundsman);//agregué el token encriptado
api.put('/updateRoundsmanP/:id',md_auth.ensureAuth,RoundsmanController.updateRoundsmanPassword);//agregué el token encriptado
api.delete('/roundsman/:id',md_auth.ensureAuth,RoundsmanController.deleteRoundsman);//agregué el token encriptado
api.get('/get-image-roundsman/:imageFile', RoundsmanController.getImageFile);/*:id es obligatorio*/




module.exports= api;