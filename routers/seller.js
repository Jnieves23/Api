'use strict'

var express= require ('express');
var UseController= require('../controllers/user');
var SellerController= require('../controllers/seller');

var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 
var multipart= require ('connect-multiparty'); //permite trabajar con los ficheros
var md_upload= multipart({uploadDir: './uploads/users'});//donde se van a subir los ficheros


api.get('/sellers/:page?',md_auth.ensureAuth,SellerController.getSellers);//agregué el token encriptado
api.get('/seller/:id',md_auth.ensureAuth,SellerController.getSeller);//agregué el token encriptado
api.put('/updateSeller/:id',md_auth.ensureAuth,SellerController.updateSeller);//agregué el token encriptado
api.put('/updateSellerP/:id',md_auth.ensureAuth,SellerController.updateSellerPassword);//agregué el token encriptado
api.delete('/seller/:id',md_auth.ensureAuth,SellerController.deleteSeller);//agregué el token encriptado
api.get('/get-image-seller/:imageFile', SellerController.getImageFile);/*:id es obligatorio*/

module.exports= api;