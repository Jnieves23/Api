'use strict'

var express= require ('express');
var CategoryController= require('../controllers/category');
var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 

api.get('/category/:id',CategoryController.getCategory);
api.post('/category',md_auth.ensureAuth,CategoryController.saveCategory);
api.get('/category_page/:page?',CategoryController.getCategorys_page);
api.get('/category_sin_page',CategoryController.getCategorys_sin_page);
api.put('/category/:id',md_auth.ensureAuth, CategoryController.updateCategory);
api.delete('/category/:id',md_auth.ensureAuth, CategoryController.deleteCategory);
module.exports= api;