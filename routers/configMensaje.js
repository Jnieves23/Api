'use strict'
var express= require ('express');
var ConfigMensajeController= require('../controllers/configMensaje');
var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 

api.post('/configMensaje',ConfigMensajeController.SedMail);
api.get('/configMensaje',md_auth.ensureAuth,ConfigMensajeController.getMail);
api.get('/validateMail/:email',ConfigMensajeController.ValidateMail);

api.get('/validateMail_by_register/:email',ConfigMensajeController.validateMail_by_register);

module.exports= api;
