'use strict'

var express= require ('express');
var ReceiptController= require('../controllers/receipt');

var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 

api.get('/receiptByUser/:id',md_auth.ensureAuth, ReceiptController.getReceiptByUser);//agregué el token encriptado
api.get('/receiptByOffice/:office',md_auth.ensureAuth, ReceiptController.getReceiptByUser);//agregué el token encriptado
api.get('/receiptBySeller/:seller',md_auth.ensureAuth, ReceiptController.getReceiptByUser);//agregué el token encriptado
api.get('/receiptByOrder/:order',md_auth.ensureAuth, ReceiptController.getReceiptByOrder);//agregué el token encriptado

api.get('/receiptBySellerReport/:seller',md_auth.ensureAuth, ReceiptController.getReceiptBySellerReport);//agregué el token encriptado

api.get('/receiptByOfficeReport/:office',md_auth.ensureAuth, ReceiptController.getReceiptByOfficeReport);//agregué el token encriptado
api.get('/receiptByOffice_Transference_Report/:office',md_auth.ensureAuth, ReceiptController.getReceiptByOffice_Transference_Report);//agregué el token encriptado
api.get('/receiptByBook_Report/:book',md_auth.ensureAuth, ReceiptController.getReceiptByBook_Report);//agregué el token encriptado

api.get('/receiptByBook_Report_Category/:category',md_auth.ensureAuth, ReceiptController.getReceiptByBook_Report_Category);//agregué el token encriptado


api.post('/receipt',md_auth.ensureAuth, ReceiptController.saveReceipt);//agregué el token encriptado
api.get('/receiptById/:id',md_auth.ensureAuth, ReceiptController.getReceiptById);
api.get('/isReceiptR/:id',md_auth.ensureAuth, ReceiptController.isReceiptEstadoR);
api.put('/receipt/:id',md_auth.ensureAuth, ReceiptController.updateReceipt);
api.delete('/receipt/:id',md_auth.ensureAuth, ReceiptController.deleteReceipt);//agregué el token encriptado

api.delete('/receipt_list/:id', md_auth.ensureAuth, ReceiptController.deleteReceiptWithId) ;

/*
api.get('/books/:author?',md_auth.ensureAuth, BookController.getBooks);//agregué el token encriptado
api.delete('/book/:id',md_auth.ensureAuth, BookController.deleteBook);//agregué el token encriptado
api.post('/upload-image-book/:id', [ md_auth.ensureAuth,md_upload ] , BookController.uploadImage);
api.get('/get-image-book/:imageFile', BookController.getImageFile);
api.put('/updateBook/:id',md_auth.ensureAuth, BookController.updateBook);
api.get('/booksP/:page?',md_auth.ensureAuth,BookController.getBooksP);//agregué el token encriptado
*/
module.exports= api;