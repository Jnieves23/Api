'use strict'

var express= require ('express');
var ShoppingController= require('../controllers/shopping');

var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 

api.get('/shoppingListByUser/:id',md_auth.ensureAuth, ShoppingController.getShoppingByUser);//agregué el token encriptado

module.exports= api;