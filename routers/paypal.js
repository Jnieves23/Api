'use strict'

var express= require ('express');
var PaypalController= require('../controllers/paypal');

var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 

api.post('/pay',md_auth.ensureAuth, PaypalController.SendPaypal);//agregué el token encriptado
api.get('/success',md_auth.ensureAuth, PaypalController.getSuccess);//agregué el token encriptado
api.get('/cancel',md_auth.ensureAuth, PaypalController.getcancel);//agregué el token encriptado
api.post('/response_paypal', PaypalController.postData);//agregué el token encriptado





module.exports= api;