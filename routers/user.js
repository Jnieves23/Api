'use strict'

var express= require ('express');
var UseController= require('../controllers/user');

var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 
var multipart= require ('connect-multiparty'); //permite trabajar con los ficheros
var md_upload= multipart({uploadDir: './uploads/users'});//donde se van a subir los ficheros


api.get('/probando-controlador',md_auth.ensureAuth,UseController.pruebas);//agregué el token encriptado
api.post('/register',UseController.saveUser);
api.post('/login',UseController.loginUser);
api.put('/updateUser/:id',md_auth.ensureAuth, UseController.updateUser);/*:id es obligatorio*/
api.put('/editUser/:id', UseController.updateUser_sin_permisos);/*:id es obligatorio*/
api.get('/get-image-user/:imageFile', UseController.getImageFile);/*:id es obligatorio*/

api.post('/upload-image-user/:id', [ md_auth.ensureAuth,md_upload ] , UseController.uploadImage);/*:id es obligatorio*/

api.get('/identification/:cedula',md_auth.ensureAuth, UseController.getFindIdentification);/*:id es obligatorio*/
api.post('/registerPresent', UseController.saveClientFisicamente);/*:id es obligatorio*/


api.get('/client/:id',md_auth.ensureAuth,UseController.getFindId);//agregué el token encriptado

api.get('/validate_cedula/:cedula',md_auth.ensureAuth,UseController.mValidateCedulaEcuadorian); 
api.get('/forgetPassword/:correo',UseController.forgetPassword);

module.exports= api;