'use strict'

var express= require ('express');
var ScheduleController= require('../controllers/schedule');
var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 

api.get('/schedule/:id',ScheduleController.getSchedule);
api.post('/schedule',md_auth.ensureAuth,ScheduleController.saveSchedule);
api.get('/schedules/:page?',ScheduleController.getSchedules);
api.get('/schedules_sin_page/:estado?',ScheduleController.getSchedules_sin_page);
api.put('/schedule/:id',md_auth.ensureAuth, ScheduleController.updateSchedule);
api.delete('/schedule/:id',md_auth.ensureAuth, ScheduleController.deleteSchedule);
module.exports= api;