'use strict'

var express= require ('express');
var CompanyController= require('../controllers/company');

var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 
var multipart= require ('connect-multiparty'); //permite trabajar con los ficheros
var md_upload= multipart({uploadDir: './uploads/company'});//donde se van a subir los ficheros



api.get('/company/:id',md_auth.ensureAuth,CompanyController.getCompany);
api.post('/company',md_auth.ensureAuth,CompanyController.saveCompany);
api.put('/company/:id',md_auth.ensureAuth, CompanyController.updateCompany);
api.post('/upload-image-company/:id', [ md_auth.ensureAuth,md_upload ] , CompanyController.uploadImage);/*:id es obligatorio*/
api.get('/get-image-company/:imageFile', CompanyController.getImageFile);/*:id es obligatorio*/
module.exports= api;