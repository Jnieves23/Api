'use strict'

var express= require ('express');
var AuthorController= require('../controllers/author');

var api= express.Router();
var md_auth= require('../middlewares/authenticated'); 
var multipart= require ('connect-multiparty'); //permite trabajar con los ficheros
var md_upload= multipart({uploadDir: './uploads/authors'});//donde se van a subir los ficheros



api.get('/author/:id',AuthorController.getAuthor);//agregué el token encriptado
api.post('/author',md_auth.ensureAuth,AuthorController.saveAuthor);//agregué el token encriptado
api.get('/authors/:page?',AuthorController.getAuthors);//agregué el token encriptado
api.get('/authorsDeshabilitados/:page?',AuthorController.getAuthorsDeshabilitados);//agregué el token encriptado
api.get('/authors_sPage/:estado?',AuthorController.getAuthorsSinPage);//agregué el token encriptado

api.put('/author/:id',md_auth.ensureAuth, AuthorController.updateAuthor);
api.delete('/author/:id',md_auth.ensureAuth, AuthorController.deleteAuthor);
api.post('/upload-image-author/:id', [ md_auth.ensureAuth,md_upload ] , AuthorController.uploadImage);/*:id es obligatorio*/
api.get('/get-image-author/:imageFile', AuthorController.getImageFile);/*:id es obligatorio*/


//api.get('/author/:id',md_auth.ensureAuth,AuthorController.getAuthor);//agregué el token encriptado
//api.get('/authors/:page?',md_auth.ensureAuth,AuthorController.getAuthors);//agregué el token encriptado
module.exports= api;