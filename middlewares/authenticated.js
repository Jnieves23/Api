'use strict'

var  jwt= require('jwt-simple');
var moment= require('moment');
var secret= 'clave_secreta_user';


/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function ensureAuth (req,res, next){
    if(!req.headers.authorization){
        return res.status(403).send({message: 'La petición no tiene cabecera de autenticación'})
    }
    var token= req.headers.authorization.replace(/[ '"]+/g,'');
    //decodificar el token
    try{
        var payload= jwt.decode(token,secret);
        if(payload.exp<=moment().unix()){
            return res.status(404).send({message: 'El token ha expirado'})//el usuario tendrá que autenticarse de nuevo
        }
    }
    catch(ex){
        //console.log(ex);
        return res.status(404).send({message: 'El token no es válido'})
    }
    req.user=payload;
    next();
   }

module.exports= {
    ensureAuth
}