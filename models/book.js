'use strict'

var mongoose = require('mongoose');
var Shema= mongoose.Schema;

var BookShema= Shema({
    title: String,
    description: String,
    year: Number,
    image: String,
    author: {type: Shema.ObjectId,ref:'Author'},
    category: {type: Shema.ObjectId,ref:'Category'},

//---------------------------------------------------------//    
   price: Number,    
//   office: [{type: Shema.ObjectId,ref:'Office'}],
//   quantity: Number,
//---------------------------------------------------------//
    estado:String,
    quantity:[{type: Shema.ObjectId,ref:'Quota_Book_Office'}],
    dscto:Number

});

module.exports=mongoose.model('Book',BookShema);