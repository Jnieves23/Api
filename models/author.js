'use strict'

var mongoose = require('mongoose');
var Shema= mongoose.Schema;

var AuthorShema= Shema({
    name: String,
    description: String,
    image: String,
    estado:String
});

module.exports=mongoose.model('Author',AuthorShema);