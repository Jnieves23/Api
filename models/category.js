'use strict'

var mongoose = require('mongoose');
var Shema= mongoose.Schema;

var CategoryShema= Shema({
    title: String,
    description: String,
    estado:String
});

module.exports=mongoose.model('Category',CategoryShema);