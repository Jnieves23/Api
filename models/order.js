'use strict'

var mongoose = require('mongoose');
var Shema= mongoose.Schema;

var OrderShema= Shema({
    reservation_date: String,
    expiration_date: String,
    estado: String,//  PP: pagado por Paypal || PE: pagado por efectivo  ||EP: enviado por Paypal || EE: enviado por efectivo  || R: reservado || E: entregado
    user: [{type: Shema.ObjectId,ref:'User'}],
    receipt:   [{type: Shema.ObjectId,ref:'Receipt'}],
    pay_date: String,
    pin: String,
    estado_pin: String, // G: generado ,
    total:Number,
    roundsman: {type:Shema.ObjectId,ref:'User'},
    roundsman_name:String,
    pdf_comprobante:String,
    pdf_comprobante_date:String,
    give_date: String,//fecha entregada por el repartidor
    update_date: String,//fecha actualizada por el repartidor
    commentary: String,//comentario
    destine_direction:String
});

module.exports=mongoose.model('Order',OrderShema);