'use strict'

var mongoose = require('mongoose');
var Shema= mongoose.Schema;

var UserShema= Shema({
    name: String,
    surname: String,
    email: String,
    password: String,
    role: String,
    image: String,
    office:String,
    identification: String,
    usuario: String,
    direccion:String,
    estado:String
});

module.exports=mongoose.model('User',UserShema);
