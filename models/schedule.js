'use strict'

var mongoose = require('mongoose');
var Shema= mongoose.Schema;

var SheduleShema= Shema({
    name: String,
    monday: String,
    tuesday: String,
    wednesday: String,
    thursday: String,
    friday: String,
    saturday: String,
    sunday: String,
    estado:String
});

module.exports=mongoose.model('Shedule',SheduleShema);