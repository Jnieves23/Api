'use strict'

var mongoose = require('mongoose');
var Shema= mongoose.Schema;

var Quota_Book_OfficeShema= Shema({
    book: {type: Shema.ObjectId,ref:'Book'},
 //   book: String,
    total:Number,
    office: {type: Shema.ObjectId,ref:'Office'},
    quota_stock:Number,//para las cantidades que estarán en stock
    estado:String,//para las cantidades que estarán en stock
});

module.exports=mongoose.model('Quota_Book_Office',Quota_Book_OfficeShema);