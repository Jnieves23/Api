'use strict'

var mongoose = require('mongoose');
var Shema= mongoose.Schema;

var CompanyShema= Shema({
    name: String,
    description: String,
    image: String,
});

module.exports=mongoose.model('Company',CompanyShema);