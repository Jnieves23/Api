'use strict'

var mongoose = require('mongoose');
var Shema= mongoose.Schema;

var OfficeShema= Shema({
    name: String,
    description: String,
    addres: String,
    phone: String,
    image: String,
    schedule: {type: Shema.ObjectId,ref:'Schedule'},
    company: {type: Shema.ObjectId,ref:'Company'},
    lat: Number,
    lng: Number,
    estado:String
});

module.exports=mongoose.model('Office',OfficeShema);