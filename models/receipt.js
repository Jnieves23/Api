'use strict'

var mongoose = require('mongoose');
var Shema= mongoose.Schema;

var ReceiptShema= Shema({
 /*    reservation_date: String,
    expiration_date: String, */
    estado: String,//  P: pagado || R: reservado || E: entregadas
    user: {type: Shema.ObjectId,ref:'User'}, 
    book: {type: Shema.ObjectId,ref:'Book'},
    office: {type:Shema.ObjectId,ref:'Office'},
    quota: {type: Shema.ObjectId,ref:'Quota_Book_Office'},
    name: String,
    price: Number,
    cantidad: Number,
    order: {type:Shema.ObjectId,ref:'Order'},
    transference:Boolean,
    destine_transference:String,
    roundsman: {type:Shema.ObjectId,ref:'User'},
    roundsman_name:String,
    receipt_date: String,
    receipt_expiration_date: String,
    //destine_direction:String


  
});
module.exports=mongoose.model('Receipt',ReceiptShema);
