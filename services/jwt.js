'use strict'

var  jwt= require('jwt-simple');
var moment= require('moment');
var secret= 'clave_secreta_user';
/***************
 * METODO     :
 * DESCRIPCION:
 * AUTOR      :
 * *************************** */
function createToken (user ){
    var payload= {
        sub:user._id,//id del objeto
        name: user.name,
        surname: user.surname,
        email: user.email,
        role: user.role,
        image: user.image,
        iat: moment().unix(),//fecha actual
        exp: moment().add(30,'days').unix//cada 30 dias
        
    }
    return jwt.encode(payload,secret);//tambien envio una clave secreta para hacer el hash
}

module.exports= {
    createToken
}